# Metadata validation service

Metadata Validation Service is used by Cellar to validate RDF metadata of the digital content managed by Cellar. Validation is implemented using SHACL language for validating RDF graphs against a set of condition.


## Getting started and installation

Metadata Validation Service is implemented using Spring Boot framework. It requires Java 11.

After pulling the source code, configure shapes, extended and inferred models in the application.properties by using default models from src/main/resources folder. In application.properties there will be:

```
file:
  shapes: "classpath:/shapes.rdf"
  extendedModel: "classpath:/extended_model.rdf"
  inferenceModel: "classpath:/inference_model.rdf"
```


After the configuration, create an executable jar file with Maven 'clean compile install -DskipTests' command, which will produce metadata-validation-app-2.0.2.jar under yourPath/metadata-validation-service-main/target/ directory.

To run it execute 'java -jar yourPath/metadata-validation-service-main/target/metadata-validation-app-2.0.2.jar' from a command shell.

Successfully started Metadata-Validation-Service will run an embedded Tomcat container hosted at http://localhost:8887 Metadata-Validation-Service API will then be available by Swagger interface at http://localhost:8887/validation/swagger-ui.html


## Authors
Cellar Team

## License
Copyright the European Union 2022. Licensed under the EUPL-1.2 or later.

## Project status
Live
