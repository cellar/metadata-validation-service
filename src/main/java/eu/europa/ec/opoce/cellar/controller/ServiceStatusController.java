/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.controller
 *             FILE : ServiceStatusController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 09 12, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-09-12 09:15:43 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.controller;

import eu.europa.ec.opoce.cellar.service.ServiceStatusService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

/**
 * @author ARHS Developments
 */
@Controller
public class ServiceStatusController {

    private ServiceStatusService serviceStatusService;

    @Autowired
    public ServiceStatusController(ServiceStatusService serviceStatusService) {
        this.serviceStatusService = serviceStatusService;
    }

    @Operation(summary = "Retrieve the internal configuration of the service")
    @GetMapping(value = "/status", produces = "application/json;charset=UTF-8")
    public ResponseEntity<List<Object>> getStatus() {
        List<Object> statusList = this.serviceStatusService.getServiceStatus();
        return ResponseEntity.ok(statusList);
    }
}
