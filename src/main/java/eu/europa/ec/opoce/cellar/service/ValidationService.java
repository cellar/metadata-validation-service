package eu.europa.ec.opoce.cellar.service;

import org.apache.jena.rdf.model.Model;
import org.springframework.retry.annotation.Retryable;

import java.util.ConcurrentModificationException;

public interface ValidationService {
    /**
     * This method validates the inputModel against the shapesModel and returns the validation result in a Jena Model
     * Some validation API allows the possibility to validate the shapes provided during the validation of the model.
     * For example, TopBraid SHACL API offers this possibility.
     *
     * @param inputModel the Jena Model that will be validated; this model will not be modified nor closed
     * @param shapesModel the SHACL shapes; this model will not be modified nor closed
     * @param validateShapes boolean value defining if the shapes must be validated as well.
     * @return a Jena Model containing the validation result
     */
    @Retryable(maxAttempts = 2, value = ConcurrentModificationException.class)
    Model validate(Model inputModel, Model shapesModel, boolean validateShapes);

    /**
     * Check if the validation model contains violation.
     * @param validationModel the Jena Model that returned by the {@link #validate(Model, Model, boolean)}
     * @return true iff the validation model doesn't contain violations
     */
    boolean modelPassedValidation(Model validationModel);
}
