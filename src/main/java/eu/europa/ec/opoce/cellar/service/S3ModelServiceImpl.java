package eu.europa.ec.opoce.cellar.service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3URI;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.google.common.io.CharStreams;
import eu.europa.ec.opoce.cellar.exception.S3DataException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

@Service
public class S3ModelServiceImpl implements S3ModelService {

    private final AmazonS3 amazonS3;

    @Autowired
    public S3ModelServiceImpl(AmazonS3 amazonS3) {
        this.amazonS3 = amazonS3;
    }

    @Override
    public Optional<String> getModel(String path) {

        if (StringUtils.isEmpty(path)) {
            return Optional.empty();
        }

        Optional<String> bucketFromPath = getBucketFromPath(path);
        Optional<String> objectKeyFromPath = getObjectKeyFromPath(path);

        if (bucketFromPath.isEmpty() || objectKeyFromPath.isEmpty()) {
            return Optional.empty();
        }

        return Optional.ofNullable(amazonS3.getObject(bucketFromPath.get(), objectKeyFromPath.get()))
                .map(this::asString);
    }

    private Optional<String> getBucketFromPath(String path) {
        return Optional.ofNullable(new AmazonS3URI(path).getBucket());
    }

    private Optional<String> getObjectKeyFromPath(String path) {
        return Optional.ofNullable(new AmazonS3URI(path).getKey());
    }

    private String asString(S3Object s3Object) {
        try (S3ObjectInputStream is = s3Object.getObjectContent()) {
            return CharStreams.toString(new InputStreamReader(is, StandardCharsets.UTF_8));
        } catch (IOException e) {
            throw new S3DataException("Cannot convert s3Object", e);
        }
    }
}
