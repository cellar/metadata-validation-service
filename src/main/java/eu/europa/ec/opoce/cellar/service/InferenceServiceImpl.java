package eu.europa.ec.opoce.cellar.service;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Strings;
import eu.europa.ec.opoce.cellar.configuration.FileConfiguration;
import eu.europa.ec.opoce.cellar.configuration.InferenceQueries;
import eu.europa.ec.opoce.cellar.utils.ModelUtils;
import org.apache.jena.rdf.listeners.StatementListener;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.rdf.model.impl.StatementImpl;
import org.apache.jena.update.UpdateAction;
import org.apache.jena.update.UpdateFactory;
import org.apache.jena.vocabulary.OWL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.Set;
import java.util.regex.Pattern;

@Service
public class InferenceServiceImpl implements InferenceService {

    private static final Logger LOGGER = LoggerFactory.getLogger(InferenceServiceImpl.class);
    private static final Pattern CELLAR_PATTERN = Pattern.compile("(cellar)\\/[a-z0-9-]*");

    private final String extendedModelPath;
    private final String inferenceModelPath;
    private final InferenceQueries inferenceQueries;
    private final Model extendedModel;
    private final Model inferenceModel;

    @Autowired
    public InferenceServiceImpl(FileConfiguration fileConfiguration, InferenceQueries inferenceQueries,
                                ConfigurationLoader loader) {
        this.extendedModelPath = fileConfiguration.getExtendedModel();
        this.extendedModel = getExtendedModel(loader, extendedModelPath);
        this.inferenceModelPath = fileConfiguration.getInferenceModel();
        this.inferenceModel = loader.getModel(inferenceModelPath);
        this.inferenceQueries = inferenceQueries;

        // make sure that we have sparql queries properly configured or raise a warning
        if (inferenceQueries.getQueries().isEmpty()) {
            LOGGER.warn("No inference SPARQL queries could be retrieved from the application.yml file, " +
                    "triples will be missing after the inference");
        }
    }

    private Model getExtendedModel(ConfigurationLoader loader, String resource) {
        if (Strings.isNullOrEmpty(resource)) {
            LOGGER.warn("Extended model not defined, empty model will be used.");
            return ModelFactory.createDefaultModel();
        }
        return loader.getModel(extendedModelPath, true);
    }

    @Override
    public Model infer(Model inputModel) {
        Assert.notNull(inputModel, "Input model cannot be null for the inference");

        Model outputModel = normalize(inputModel);

        String logMessage = "Inference started.";
        LOGGER.debug(logMessage);
        long startTime = System.currentTimeMillis();
        Model allModelForInference = ModelFactory.createDefaultModel();
        allModelForInference.add(outputModel);
        allModelForInference.add(inferenceModel);

        UpdateModelListener listener = new UpdateModelListener(outputModel);
        allModelForInference.register(listener);
        for (String sparqlQuery : inferenceQueries.getQueries()) {
            LOGGER.debug("Executing SPARQL on model for the inference: \n{}", sparqlQuery);
            UpdateAction.execute(UpdateFactory.create(sparqlQuery), allModelForInference);
        }
        allModelForInference.unregister(listener);
        allModelForInference.close();

        LOGGER.debug("Inference finished, total time : {} ms.", (System.currentTimeMillis() - startTime));
        outputModel.add(extendedModel);

        return outputModel;
    }

    @Override
    public Model normalize(Model inputModel) {
        Assert.notNull(inputModel, "Input model for normalization cannot be null");

        Model normalizedModel = ModelFactory.createDefaultModel();
        StmtIterator stmtIterator = inputModel.listStatements();
        while (stmtIterator.hasNext()) {
            Statement statement = stmtIterator.nextStatement();
            Resource subject = statement.getSubject();
            Property property = statement.getPredicate();
            RDFNode object = statement.getObject();

            if (!property.equals(OWL.sameAs)) {
                if (subject.isURIResource()) {
                    subject = inputModel.getResource(
                            getTechnicalIdentifierOrDefault(subject.getURI(), inputModel));
                }

                if (object.isURIResource()) {
                    object = inputModel.getResource(
                            getTechnicalIdentifierOrDefault(object.asResource().getURI(), inputModel));
                }
            }
            normalizedModel.add(new StatementImpl(subject, property, object));
        }

        return normalizedModel;
    }

    private String getTechnicalIdentifierOrDefault(String uri, Model model) {
        Set<String> sameAses = ModelUtils.getSameAses(uri, model);
        for (String sameAs : sameAses) {
            if (CELLAR_PATTERN.matcher(sameAs).find()) {
                return sameAs;
            }
        }
        return uri;
    }

    @Override
    public Object getStatus() {
        return new InferenceServiceStatus();
    }

    private static class UpdateModelListener extends StatementListener {

        private final Model model;

        private UpdateModelListener(Model model) {
            this.model = model;
        }

        @Override
        public void addedStatement(Statement s) {
            model.add(s);
        }

        @Override
        public void removedStatement(Statement s) {
            model.remove(s);
        }
    }

    public class InferenceServiceStatus {
        InferenceServiceStatus() {
        }

        @JsonProperty(value = "extendedModelPath")
        public String getExtendedModel() {
            return extendedModelPath;
        }

        @JsonProperty(value = "inferenceModelPath")
        public String getInferredOntology() {
            return inferenceModelPath;
        }

        @JsonProperty(value = "inferenceQueries")
        public InferenceQueries getInferenceQueries() {
            return inferenceQueries;
        }
    }
}
