package eu.europa.ec.opoce.cellar.configuration;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Swagger3Config {
    private final String projectVersion;
    private final String description;

    public Swagger3Config(@Value("${springdoc.version}") String projectVersion, @Value("${springdoc.description}") String description) {
        this.projectVersion = projectVersion;
        this.description=description;
    }
    @Bean
    public OpenAPI openApi() {
        if(description!=null&&!description.isEmpty()) {
            return new OpenAPI().info(new Info()
                    .description(description)
                    .version(projectVersion));
        }else{
            return new OpenAPI().info(new Info()
                    .description(description));

        }

    }

}
