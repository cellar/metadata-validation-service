/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.service
 *             FILE : ServiceStatusRetrievable.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 10 11, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-10-11 15:59:24 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.service;

/**
 * @author ARHS Developments
 */
public interface ServiceStatusRetrievable {
    /**
     * This method returns an Object with information on the classes implementing it.
     * This returned Object will be serialized as JSON.
     * @return an Object class containing internal information to be displayed
     */
    Object getStatus();
}
