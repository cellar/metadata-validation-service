/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.exception
 *             FILE : MissingTestingShapesModelException.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 09 08, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-09-08 09:03:05 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.exception;

/**
 * @author ARHS Developments
 */
public class BadRequestException extends Exception {

    public BadRequestException(String message, Throwable throwable) {
        super(message + ", reason: " + throwable.getMessage(), throwable);
    }

    public BadRequestException(String message) {
        super(message);
    }
}
