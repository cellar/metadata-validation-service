package eu.europa.ec.opoce.cellar.exception;

import eu.europa.ec.opoce.cellar.configuration.ParserConfiguration;
import org.apache.commons.lang3.StringUtils;
import org.apache.jena.riot.RiotException;
import org.apache.jena.riot.system.ErrorHandler;
import org.apache.jena.riot.system.ErrorHandlerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.regex.Pattern;

import static org.apache.jena.riot.SysRIOT.fmtMessage;

@Component
public class ParserErrorHandler implements ErrorHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ParserConfiguration.class);
    private final ErrorHandler errorHandler;
    private final List<Pattern> exceptionPatterns;

    @Autowired
    public ParserErrorHandler(final ParserConfiguration parserConfiguration) {
        this(parserConfiguration, ErrorHandlerFactory.errorHandlerStd);
    }

    public ParserErrorHandler(final ParserConfiguration parserConfiguration, ErrorHandler baseErrorHandler) {
        exceptionPatterns = parserConfiguration.getCompiledExceptionPatterns();
        errorHandler = baseErrorHandler;
    }

    @Override
    public void warning(final String message, final long line, final long col) {
        if (StringUtils.isBlank(message)) {
            return;
        }

        if (shouldThrowException(message)) {
            throw new RiotException(fmtMessage(message, line, col));
        } else {
            errorHandler.warning(message, line, col);
        }
    }

    @Override
    public void error(final String message, final long line, final long col) {
        errorHandler.error(message, line, col);
    }

    @Override
    public void fatal(final String message, final long line, final long col) {
        errorHandler.fatal(message, line, col);
    }

    private boolean shouldThrowException(final String message) {
        return exceptionPatterns
                .stream()
                .filter(p -> p.matcher(message).find())
                .findFirst() // deterministic
                .map(p -> {
                    LOGGER.debug("Message \"{}\" matched pattern \"{}\"", message, p);
                    return p;
                }).isPresent();
    }
}
