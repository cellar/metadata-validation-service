package eu.europa.ec.opoce.cellar.service;

import com.fasterxml.jackson.annotation.JsonProperty;
import eu.europa.ec.opoce.cellar.configuration.FileConfiguration;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ShapeServiceImpl implements ShapeService {

    private static final Logger LOG = LogManager.getLogger(ShapeServiceImpl.class);

    private final FileConfiguration fileConfiguration;
    private final Model shapesModel;

    @Autowired
    public ShapeServiceImpl(FileConfiguration fileConfiguration, ConfigurationLoader loader) {
        this.fileConfiguration = fileConfiguration;
        this.shapesModel = loader.getModel(fileConfiguration.getShapes(), true);
    }

    @Override
    public Model getCopy() {
        Model shapes = ModelFactory.createDefaultModel();
        shapes.add(shapesModel);
        return shapes;
    }

    @Override
    public Object getStatus() {
        return new ShapeServiceStatus();
    }

    public class ShapeServiceStatus {
        ShapeServiceStatus() {
        }

        @JsonProperty(value = "shapesModelPath")
        public String getShapesModelPath() {
            return fileConfiguration.getShapes();
        }
    }
}
