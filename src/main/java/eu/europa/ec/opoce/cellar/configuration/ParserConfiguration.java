package eu.europa.ec.opoce.cellar.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import java.util.stream.Collectors;

public class ParserConfiguration {

    private static final Logger LOGGER = LoggerFactory.getLogger(ParserConfiguration.class);

    private List<String> exceptionPatterns = new ArrayList<>();

    public void setExceptionPatterns(final List<String> exceptionPatterns) {
        this.exceptionPatterns = exceptionPatterns;
    }

    public List<String> getExceptionPatterns() {
        return exceptionPatterns;
    }

    public List<Pattern> getCompiledExceptionPatterns() {
        try {
            return exceptionPatterns.stream()
                    .map(Pattern::compile)
                    .collect(Collectors.toList());
        } catch (final PatternSyntaxException e) {
            LOGGER.error("Failed to convert the exception patterns: {}", e);
            return Collections.emptyList();
        }
    }
}
