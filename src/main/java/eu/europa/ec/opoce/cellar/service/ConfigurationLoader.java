package eu.europa.ec.opoce.cellar.service;

import com.google.common.base.Charsets;
import com.google.common.io.CharStreams;
import eu.europa.ec.opoce.cellar.utils.ModelUtils;
import org.apache.jena.atlas.json.JsonParseException;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RiotException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

import static java.util.Optional.of;

/**
 * @author ARHS Development
 */
@Component
public class ConfigurationLoader {

    private static final Logger LOG = LogManager.getLogger(ConfigurationLoader.class);

    private final S3ModelService s3ModelService;

    @Autowired
    public ConfigurationLoader(S3ModelService s3ModelService) {
        this.s3ModelService = s3ModelService;
    }

    /**
     * Retrieves model from various location:
     * - classpath
     * - URL
     * - local file
     *
     * @param resource the location of the resource
     * @return the model
     */
    public Model getModel(String resource) {
        return getModel(resource, false);
    }

    /**
     * Retrieves model from various location:
     * - classpath
     * - URL
     * - local file
     *
     * @param resource the location of the resource
     * @return the model
     */
    public Model getModel(String resource, boolean allowEmpty) {
        Assert.notNull(resource, "Cannot load model with null file reference.");
        final Lang lang = Optional.ofNullable(ModelUtils.getInputFormatFromName(resource))
                .orElseThrow(() -> new IllegalArgumentException("Cannot found the lang for " + resource));
        if (isUrlBasedResource(resource)) {
            return getModelFromUrl(resource, lang)
                    .or(() -> allowEmpty ? of(ModelFactory.createDefaultModel()) : Optional.empty())
                    .orElseThrow(() -> new IllegalArgumentException("Cannot parse content from file : " + resource));
        } else if (isClasspathResource(resource)) {
            return getModelFromClasspath(resource, lang)
                    .or(() -> allowEmpty ? of(ModelFactory.createDefaultModel()) : Optional.empty())
                    .orElseThrow(() -> new IllegalArgumentException("Cannot open " + resource));
        }

        final Path path = Paths.get(resource);
        Assert.notNull(path.getFileName(), "Invalid filename for path : " + path.toString());

        LOG.info("Shapes loading from file {}.", path);
        try {
            return load(Files.readString(path), lang.getLabel());
        } catch (IOException ioe) {
            throw new UncheckedIOException("Cannot read content from file : " + path.toAbsolutePath(), ioe);
        }
    }

    private static boolean isUrlBasedResource(String resource) {
        return Optional.ofNullable(resource)
                .map(s -> {
                    String l = s.toLowerCase();
                    return l.startsWith("https:") || l.startsWith("http");
                }).orElse(false);
    }

    private static boolean isClasspathResource(String resource) {
        return Optional.ofNullable(resource)
                .map(s -> s.startsWith("classpath:"))
                .orElse(false);
    }

    private Optional<Model> getModelFromUrl(String path, Lang lang) {
        return s3ModelService.getModel(path)
                .flatMap(s1 -> ModelUtils.parseModel(s1, lang));
    }

    private Optional<Model> getModelFromClasspath(String resource, Lang lang) {
        final String r = resource.replace("classpath:", "");
        try (InputStream is = new ClassPathResource(r).getInputStream()) {
            return ModelUtils.parseModel(CharStreams.toString(new InputStreamReader(is, Charsets.UTF_8)), lang);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private static Model load(String model, String type) {
        try {
            Model parsedShape = ModelFactory.createDefaultModel();
            parsedShape.read(new StringReader(model), "", type);
            return parsedShape;
        } catch (JsonParseException | RiotException re) {
            throw new IllegalStateException("Model argument could not be parsed with type '" + type, re);
        }
    }
}
