package eu.europa.ec.opoce.cellar.configuration;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import eu.europa.ec.opoce.cellar.CoverageIgnore;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@CoverageIgnore
@Configuration
@EnableConfigurationProperties
@ConfigurationProperties("validator")
public class MetadataValidatorConfiguration {

    @Bean
    @ConfigurationProperties(prefix = "file")
    public FileConfiguration fileConfiguration() {
        return new FileConfiguration();
    }

    @Bean
    @ConfigurationProperties(prefix = "inference")
    public InferenceQueries inferenceQueries() {
        return new InferenceQueries();
    }

    @Bean
    public AmazonS3 amazonS3(AmazonS3Configuration amazonS3Configuration) {
        String accessKey = amazonS3Configuration.getAccessKey();
        String secretKey = amazonS3Configuration.getSecretKey();

        AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);

        AmazonS3ClientBuilder amazonS3ClientBuilder = AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withPathStyleAccessEnabled(true)
                .withRegion(Regions.EU_WEST_1);

        return amazonS3ClientBuilder.build();
    }

    @Bean
    @ConfigurationProperties(prefix = "aws")
    public AmazonS3Configuration amazonS3Configuration() {
        return new AmazonS3Configuration();
    }

    @Bean
    @ConfigurationProperties(prefix = "parser")
    public ParserConfiguration parserConfiguration() {
        return new ParserConfiguration();
    }
}
