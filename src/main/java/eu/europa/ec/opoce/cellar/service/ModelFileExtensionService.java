package eu.europa.ec.opoce.cellar.service;

import org.apache.jena.riot.Lang;

import java.util.Collection;

public interface ModelFileExtensionService {

    boolean isSupported(String extension);

    Collection<String> getSupportedExtensions();

    Lang getLang(String extension);
}
