/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.service
 *             FILE : StatusReportImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 09 12, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-09-12 09:20:48 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.service;

import com.google.common.annotations.VisibleForTesting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author ARHS Developments
 */
@Service
public class ServiceStatusServiceImpl implements ServiceStatusService {

    private final List<ServiceStatusRetrievable> serviceStatusRetrievableList;

    @Autowired
    public ServiceStatusServiceImpl(List<ServiceStatusRetrievable> serviceStatusRetrievableList) {
        this.serviceStatusRetrievableList = serviceStatusRetrievableList;
    }

    @Override
    public List<Object> getServiceStatus() {
        return this.getServiceStatusRetrievableList().stream()
                .map(ServiceStatusRetrievable::getStatus).collect(Collectors.toList());
    }

    @VisibleForTesting
    public List<ServiceStatusRetrievable> getServiceStatusRetrievableList() {
        return this.serviceStatusRetrievableList;
    }
}
