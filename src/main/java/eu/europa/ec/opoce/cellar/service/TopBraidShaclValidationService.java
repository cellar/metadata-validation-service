package eu.europa.ec.opoce.cellar.service;

import eu.europa.ec.opoce.cellar.utils.ModelUtils;
import org.apache.jena.query.*;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.vocabulary.OWL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.topbraid.shacl.validation.ValidationUtil;

@Service
public class TopBraidShaclValidationService implements ValidationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TopBraidShaclValidationService.class);
    private static final Query VIOLATION_COUNT_QUERY
            = QueryFactory.create("select (count(?s) as ?c) where {?s ?p <http://www.w3.org/ns/shacl#Violation>}");

    @Override
    public Model validate(Model inputModel, Model shapesModel, boolean validateShapes) {
        Assert.notNull(inputModel, "Input model cannot be null for the validation");
        Assert.notNull(shapesModel, "shapes model cannot be null for the validation");

        String logMessage = "Validation of the model against shapes started.";
        LOGGER.debug(logMessage);
        long startTime = System.currentTimeMillis();
        Resource validationReport = ValidationUtil.validateModel(inputModel, shapesModel, validateShapes);
        addSameAsOfFocusNode(inputModel, validationReport.getModel());
        LOGGER.debug("Validation of the model against shapes finished in {} ms.", (System.currentTimeMillis() - startTime));

        return validationReport.getModel();
    }

    void addSameAsOfFocusNode(Model inputModel, Model validationModel) {
        for (String focusNode : ModelUtils.getFocusNodes(validationModel)) {
            for (String sameAs: ModelUtils.getSameAses(focusNode, inputModel)) {
                if (!focusNode.equals(sameAs)) {
                    validationModel.add(ResourceFactory.createResource(focusNode), OWL.sameAs, sameAs);
                    LOGGER.debug("Added sameAs '{}' of focus node '{}'", sameAs, focusNode);
                }
            }
        }
    }

    @Override
    public boolean modelPassedValidation(Model validationModel) {
        if (validationModel.isEmpty()) {
            return true;
        }
        try (QueryExecution queryExecution = QueryExecutionFactory.create(VIOLATION_COUNT_QUERY, validationModel)) {
            ResultSet resultSet = queryExecution.execSelect();
            if (resultSet.hasNext()) {
                QuerySolution querySolution = resultSet.nextSolution();
                Literal literal = querySolution.getLiteral("?c");
                if (literal != null && literal.isLiteral() && literal.getInt() > 0) {
                    return false;
                }
            }
        }
        return true;
    }
}
