/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.service
 *             FILE : StatusReportService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 09 12, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-09-12 09:20:06 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.service;

import java.util.List;

/**
 * @author ARHS Developments
 */
public interface ServiceStatusService {
    /**
     * This method goes through the list of all {@link ServiceStatusRetrievable} defined in the application and get
     * information on their internal information like health and configuration.

     * @return a list of Object containing information about the configuration and the health of the application
     */
    List<Object> getServiceStatus();
}
