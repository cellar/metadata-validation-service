package eu.europa.ec.opoce.cellar.utils;

import org.apache.commons.lang3.StringUtils;
import org.apache.jena.graph.Node;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFLanguages;
import org.apache.jena.riot.RiotException;
import org.apache.jena.sparql.core.Var;
import org.apache.jena.sparql.engine.binding.Binding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public final class ModelUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(ModelUtils.class);
    private static final Property FOCUS_NODE_PROPERTY = ResourceFactory.createProperty("http://www.w3.org/ns/shacl#focusNode");

    private ModelUtils() {
    }

    public static Optional<Model> parseModel(String model, Lang langType) {
        if (StringUtils.isBlank(model) || langType == null) {
            return Optional.empty();
        }

        try {
            Model outputModel = ModelFactory.createDefaultModel();
            outputModel.read(new StringReader(model), "", langType.getLabel());
            return Optional.of(outputModel);
        } catch (RiotException re) {
            throw new IllegalArgumentException("Cannot parse model", re);
        }
    }

    public static Optional<Model> parseModel(Path path) {
        if (path == null || !path.toFile().exists()) {
            return Optional.empty();
        }

        Lang langType = getInputFormatFromName(path.toString());
        if (langType == null) {
            return Optional.empty();
        }

        try {
            String content = Files.readString(path);
            return parseModel(content, langType);
        } catch (IOException ioe) {
            LOGGER.warn("The model at '{}' could not be read, reason: {}", path, ioe);
            return Optional.empty();
        }
    }

    public static String writeModel(Model model, Lang langType) {
        if (model == null) {
            return "";
        }
        if (langType == null) {
            langType = Lang.RDFJSON;
        }

        StringWriter stringWriter = new StringWriter();
        model.write(stringWriter, langType.getLabel());
        return stringWriter.toString();
    }

    public static String convertModel(String model, Lang from, Lang to) {
        return parseModel(model, from)
                .map(m -> ModelUtils.writeModel(m, to))
                .orElse("");
    }

    public static Lang getInputFormatFromName(String name) {
        Lang lang = RDFLanguages.filenameToLang(name);
        if (lang != null) {
            LOGGER.debug("Name '{}' found format '{}'", name, lang.getLabel());
        } else {
            LOGGER.warn("Couldn't find a lang name for name '{}'", name);
        }
        return lang;
    }

    public static Set<String> getSameAses(final String subjectUri, final Model model) {
        final Set<String> sameAses = new HashSet<>();
        Query query = QueryFactory.create("" +
                "PREFIX owl:<http://www.w3.org/2002/07/owl#>\n" +
                "SELECT ?sameAsUri WHERE {\n" +
                "<" + subjectUri + "> (owl:sameAs|^owl:sameAs)* ?sameAsUri\n" +
                "}");

        try (QueryExecution queryExecution = QueryExecutionFactory.create(query, model)) {
            ResultSet resultSet = queryExecution.execSelect();
            while (resultSet.hasNext()) {
                Binding binding = resultSet.nextBinding();
                Node node = binding.get(Var.alloc("sameAsUri"));
                if (node.isURI()) {
                    sameAses.add(node.getURI());
                }
            }
        }

        return sameAses;
    }

    public static Set<String> getFocusNodes(final Model model) {
        return model.listObjectsOfProperty(FOCUS_NODE_PROPERTY)
                .mapWith(RDFNode::toString)
                .toSet();
    }

    public static void closeQuietly(Model model) {
        if (model != null && !model.isClosed()) {
            model.close();
        }
    }

}
