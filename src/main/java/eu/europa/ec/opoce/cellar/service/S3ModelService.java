package eu.europa.ec.opoce.cellar.service;

import java.util.Optional;

/**
 * @author ARHS Developments
 */
public interface S3ModelService {

    Optional<String> getModel(String path);
}
