/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.controller
 *             FILE : TestingValidationResult.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 09 07, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-09-07 14:27:51 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;
import java.util.UUID;

/**
 * @author ARHS Developments
 */
public class TestingMetadataValidationResult extends MetadataValidationResult {
    @JsonProperty(value = "shapesArgProvided")
    private boolean shapesArgProvided;

    @JsonProperty(value = "validShapesProvided")
    private boolean validShapesProvided;

    @JsonProperty(value = "validateShapes")
    private boolean validateShapes;

    @JsonProperty(value = "mustBeInferred")
    private boolean mustBeInferred;

    public TestingMetadataValidationResult() {
        validShapesProvided = false;
        shapesArgProvided = false;
        validateShapes = false;
        mustBeInferred = false;
    }

    public TestingMetadataValidationResult(UUID uuid) {
        super(uuid);
        validShapesProvided = false;
        shapesArgProvided = false;
        validateShapes = false;
        mustBeInferred = false;
    }

    public boolean isShapesArgProvided() {
        return shapesArgProvided;
    }

    public void setShapesArgProvided(boolean shapesArgProvided) {
        this.shapesArgProvided = shapesArgProvided;
    }

    public boolean isValidShapesProvided() {
        return validShapesProvided;
    }

    public void setValidShapesProvided(boolean validShapesProvided) {
        this.validShapesProvided = validShapesProvided;
    }

    public boolean isValidateShapes() {
        return validateShapes;
    }

    public void setValidateShapes(boolean validateShapes) {
        this.validateShapes = validateShapes;
    }

    public boolean isMustBeInferred() {
        return mustBeInferred;
    }

    public void setMustBeInferred(boolean mustBeInferred) {
        this.mustBeInferred = mustBeInferred;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        TestingMetadataValidationResult that = (TestingMetadataValidationResult) o;
        return shapesArgProvided == that.shapesArgProvided &&
                validShapesProvided == that.validShapesProvided &&
                validateShapes == that.validateShapes &&
                mustBeInferred == that.mustBeInferred;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), shapesArgProvided, validShapesProvided, validateShapes, mustBeInferred);
    }

    @Override
    public String toString() {
        return "TestingMetadataValidationResult{" +
                super.toString() +
                "shapesArgProvided=" + shapesArgProvided +
                ", validShapesProvided=" + validShapesProvided +
                ", validateShapes=" + validateShapes +
                ", mustBeInferred=" + mustBeInferred +
                '}';
    }
}
