package eu.europa.ec.opoce.cellar.exception;

public class S3DataException extends RuntimeException {

    public S3DataException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
