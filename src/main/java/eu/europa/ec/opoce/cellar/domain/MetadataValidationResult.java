package eu.europa.ec.opoce.cellar.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;
import java.util.UUID;

public class MetadataValidationResult {
    @JsonProperty(value = "requestId")
    private UUID requestId;

    @JsonProperty(value = "errorMessage")
    private String errorMessage;

    @JsonProperty(value = "validationResult")
    private String validationResult;

    @JsonProperty(value = "validJenaModelProvided")
    private boolean validJenaModelProvided;

    @JsonProperty(value = "conformModelProvided")
    private boolean conformModelProvided;

    @JsonProperty(value = "totalTimeForInferenceInMilliSeconds")
    private long totalTimeForInferenceInMilliSeconds;

    @JsonProperty(value = "totalTimeForValidationInMilliSeconds")
    private long totalTimeForValidationInMilliSeconds;

    MetadataValidationResult() {
        errorMessage = "";
        validationResult = "";
        validJenaModelProvided = false;
        totalTimeForInferenceInMilliSeconds = -1;
        totalTimeForValidationInMilliSeconds = -1;
        conformModelProvided = false;
    }

    public MetadataValidationResult(UUID requestId) {
        this();
        this.requestId = requestId;
    }

    public UUID getRequestId() {
        return requestId;
    }

    public void setRequestId(UUID requestId) {
        this.requestId = requestId;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getValidationResult() {
        return validationResult;
    }

    public void setValidationResult(String validationResult) {
        this.validationResult = validationResult;
    }

    public boolean isValidJenaModelProvided() {
        return validJenaModelProvided;
    }

    public void setValidJenaModelProvided(boolean validJenaModelProvided) {
        this.validJenaModelProvided = validJenaModelProvided;
    }

    public boolean conformModelProvided() {
        return conformModelProvided;
    }

    public void setConformModelProvided(boolean conformModelProvided) {
        this.conformModelProvided = conformModelProvided;
    }

    public long getTotalTimeForInferenceInMilliSeconds() {
        return totalTimeForInferenceInMilliSeconds;
    }

    public void setTotalTimeForInferenceInMilliSeconds(long totalTimeForInferenceInMilliSeconds) {
        this.totalTimeForInferenceInMilliSeconds = totalTimeForInferenceInMilliSeconds;
    }

    public long getTotalTimeForValidationInMilliSeconds() {
        return totalTimeForValidationInMilliSeconds;
    }

    public void setTotalTimeForValidationInMilliSeconds(long totalTimeForValidationInMilliSeconds) {
        this.totalTimeForValidationInMilliSeconds = totalTimeForValidationInMilliSeconds;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MetadataValidationResult that = (MetadataValidationResult) o;
        return validJenaModelProvided == that.validJenaModelProvided &&
                conformModelProvided == that.conformModelProvided &&
                totalTimeForInferenceInMilliSeconds == that.totalTimeForInferenceInMilliSeconds &&
                totalTimeForValidationInMilliSeconds == that.totalTimeForValidationInMilliSeconds &&
                Objects.equals(requestId, that.requestId) &&
                Objects.equals(errorMessage, that.errorMessage) &&
                Objects.equals(validationResult, that.validationResult);
    }

    @Override
    public int hashCode() {
        return Objects.hash(requestId, errorMessage, validationResult, validJenaModelProvided, conformModelProvided, totalTimeForInferenceInMilliSeconds, totalTimeForValidationInMilliSeconds);
    }

    @Override
    public String toString() {
        return "MetadataValidationResult{" +
                "requestId=" + requestId +
                ", errorMessage='" + errorMessage + '\'' +
                ", validationResult='" + validationResult + '\'' +
                ", validJenaModelProvided=" + validJenaModelProvided +
                ", conformModelProvided=" + conformModelProvided +
                ", totalTimeForInferenceInMilliSeconds=" + totalTimeForInferenceInMilliSeconds +
                ", totalTimeForValidationInMilliSeconds=" + totalTimeForValidationInMilliSeconds +
                '}';
    }
}
