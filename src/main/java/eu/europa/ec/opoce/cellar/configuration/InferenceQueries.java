package eu.europa.ec.opoce.cellar.configuration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class InferenceQueries {

    private List<String> queries;

    public InferenceQueries() {
        queries = new ArrayList<>();
    }

    public List<String> getQueries() {
        return queries;
    }

    public void setQueries(List<String> queries) {
        if (queries == null) {
            queries = new ArrayList<>();
        }
        this.queries = queries;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        InferenceQueries that = (InferenceQueries) o;
        List<String> thatQueries = that.getQueries();
        Collections.sort(queries);
        Collections.sort(thatQueries);
        return queries.equals(thatQueries);
    }

    @Override
    public int hashCode() {
        Collections.sort(queries);
        return Objects.hash(queries);
    }
}
