package eu.europa.ec.opoce.cellar.service;

import org.apache.jena.rdf.model.Model;

public interface ShapeService extends ServiceStatusRetrievable {
    /**
     * This method returns a copy of the shapes loaded at the initialization of the service.
     * The copy is required to prevent the closing of the shapes Jena Model in a calling method.
     * This copy should be managed and closed by the calling method.
     *
     * @return a copy of the Jena Model containing the shapes loaded by the service
     */
    Model getCopy();
}
