package eu.europa.ec.opoce.cellar.configuration;

public class FileConfiguration {

    private String shapes;
    private String extendedModel;
    private String inferenceModel;

    public String getShapes() {
        return shapes;
    }

    public void setShapes(String shapes) {
        this.shapes = shapes;
    }

    public String getExtendedModel() {
        return extendedModel;
    }

    public void setExtendedModel(String extendedModel) {
        this.extendedModel = extendedModel;
    }

    public String getInferenceModel() {
        return inferenceModel;
    }

    public void setInferenceModel(String inferenceModel) {
        this.inferenceModel = inferenceModel;
    }
}
