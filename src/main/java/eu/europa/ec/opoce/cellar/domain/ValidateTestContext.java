/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.domain
 *             FILE : ValidateTestContext.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 12 20, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-12-20 09:55:27 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.domain;

/**
 * @author ARHS Developments
 */
public class ValidateTestContext {
    private String model;
    private String shapes;
    private boolean infer;
    private boolean validateShapes;

    public ValidateTestContext() {
        infer = true;
        validateShapes = false;
        model = "";
        shapes = "";
    }
    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getShapes() {
        return shapes;
    }

    public void setShapes(String shapes) {
        this.shapes = shapes;
    }

    public boolean isInfer() {
        return infer;
    }

    public void setInfer(boolean infer) {
        this.infer = infer;
    }

    public boolean isValidateShapes() {
        return validateShapes;
    }

    public void setValidateShapes(boolean validateShapes) {
        this.validateShapes = validateShapes;
    }
}
