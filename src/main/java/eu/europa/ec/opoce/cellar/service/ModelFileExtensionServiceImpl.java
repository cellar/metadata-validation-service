package eu.europa.ec.opoce.cellar.service;

import com.google.common.collect.ImmutableMap;
import org.apache.jena.riot.Lang;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Map;

@Service
public class ModelFileExtensionServiceImpl implements ModelFileExtensionService {

    private static final Map<String, Lang> supportedFileExtensionJenaModelMap = ImmutableMap.<String, Lang>builder()
            .put("ttl", Lang.TURTLE)
            .put("rdf", Lang.RDFXML)
            .put("owl", Lang.RDFXML)
            .put("rj", Lang.RDFJSON)
            .put("nq", Lang.NQUADS)
            .put("jsonld", Lang.JSONLD)
            .build();

    @Override
    public boolean isSupported(final String extension) {
        return supportedFileExtensionJenaModelMap.containsKey(extension);
    }

    @Override
    public Collection<String> getSupportedExtensions() {
        return supportedFileExtensionJenaModelMap.keySet();
    }

    @Override
    public Lang getLang(final String extension) {
        return supportedFileExtensionJenaModelMap.get(extension);
    }

}
