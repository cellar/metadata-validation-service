package eu.europa.ec.opoce.cellar.controller;

import eu.europa.ec.opoce.cellar.domain.MetadataValidationResult;
import eu.europa.ec.opoce.cellar.domain.TestingMetadataValidationResult;
import eu.europa.ec.opoce.cellar.domain.ValidateContext;
import eu.europa.ec.opoce.cellar.domain.ValidateTestContext;
import eu.europa.ec.opoce.cellar.exception.BadRequestException;
import eu.europa.ec.opoce.cellar.exception.ParserErrorHandler;
import eu.europa.ec.opoce.cellar.service.InferenceService;
import eu.europa.ec.opoce.cellar.service.ModelFileExtensionService;
import eu.europa.ec.opoce.cellar.service.ShapeService;
import eu.europa.ec.opoce.cellar.service.ValidationService;
import eu.europa.ec.opoce.cellar.utils.ModelUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.jena.atlas.json.JsonParseException;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFParser;
import org.apache.jena.riot.RiotException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.StringReader;
import java.text.Normalizer;
import java.util.UUID;

import static eu.europa.ec.opoce.cellar.utils.ModelUtils.convertModel;

@RestController
@RequestMapping(value = "/validate", produces = "application/json;charset=UTF-8")
@Tag(name = "validation")
public class ValidationController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ValidationController.class);

    private ValidationService validationService;
    private ShapeService shapeService;
    private InferenceService inferenceService;
    private ModelFileExtensionService modelFileExtensionService;
    private ParserErrorHandler parserErrorHandler;

    @Autowired
    public ValidationController(ValidationService validationService,
                                ShapeService shapeService,
                                InferenceService inferenceService,
                                ModelFileExtensionService modelFileExtensionService,
                                ParserErrorHandler parserErrorHandler) {
        this.validationService = validationService;
        this.shapeService = shapeService;
        this.inferenceService = inferenceService;
        this.modelFileExtensionService = modelFileExtensionService;
        this.parserErrorHandler = parserErrorHandler;
    }

    @Operation(summary = "Test the validation of a model against the shapes and other possible configurations")
    @PostMapping(value = "/test/file", consumes = "multipart/form-data")
    public ResponseEntity<TestingMetadataValidationResult> validate(@RequestParam(value = "modelFile") MultipartFile modelFile,
                                                                    @RequestParam(value = "shapesFile") MultipartFile shapesFile,
                                                                    @RequestParam(value = "infer", defaultValue = "true") boolean infer,
                                                                    @RequestParam(value = "validateShapes", defaultValue = "true") boolean validateShapes) {
        UUID uuid = UUID.randomUUID();
        TestingMetadataValidationResult testingMetadataValidationResult = new TestingMetadataValidationResult(uuid);
        ValidateTestContext validateTestContext = new ValidateTestContext();
        validateTestContext.setInfer(infer);
        validateTestContext.setValidateShapes(validateShapes);
        try {
            validateTestContext.setModel(readMultipartFile(modelFile));
            validateTestContext.setShapes(readMultipartFile(shapesFile));
            return validate(validateTestContext);
        } catch (IOException ioe) {
            testingMetadataValidationResult.setErrorMessage("Input file could not be read : " + ioe);
            return ResponseEntity.badRequest().body(testingMetadataValidationResult);
        } catch (BadRequestException e) {
            testingMetadataValidationResult.setErrorMessage(e.getMessage());
            return ResponseEntity.badRequest().body(testingMetadataValidationResult);
        }
    }
    
    private String readMultipartFile(MultipartFile file) throws IOException, BadRequestException {
        String fileName = file.getOriginalFilename();
        String extension = FilenameUtils.getExtension(fileName);
        String content = new String(file.getBytes());

        if (StringUtils.isBlank(extension)) {
            throw new BadRequestException("Type of file cannot be deduced from filename, filename without extension : " + extension);
        }

        if (!modelFileExtensionService.isSupported(extension)) {
            throw new BadRequestException("The extension '" + extension + "' is not in the list of supported format : " + modelFileExtensionService.getSupportedExtensions());
        }

        return convertModel(content, modelFileExtensionService.getLang(extension), Lang.RDFJSON);
    }

    @Operation(summary = "Infer the given Jena Model in RDF/JSON format and validate it with SHACL")
    @PostMapping
    public ResponseEntity<MetadataValidationResult> validate(@RequestBody ValidateContext validateContext) {
        UUID uuid = UUID.randomUUID();
        LOGGER.info("Validation started on endpoint '/validate' for request id : {}.", uuid);
        MetadataValidationResult metadataValidationResult = new MetadataValidationResult(uuid);

        Model model = null;
        Model inferredModel = null;
        Model shapesModel = null;
        try {
            model = processModelArgument(validateContext.getModel(), metadataValidationResult);
            shapesModel = shapeService.getCopy();
            inferredModel = inferModel(model, metadataValidationResult);
            validateModel(inferredModel, shapesModel, false, metadataValidationResult);
            return ResponseEntity.ok(metadataValidationResult);
        } catch (BadRequestException bre) {
            metadataValidationResult.setErrorMessage(bre.getMessage());
            LOGGER.error("An error occurred during the validation of the model " , bre);
            return ResponseEntity.badRequest().body(metadataValidationResult);
        } catch (Exception e) {
            metadataValidationResult.setErrorMessage(e.getMessage());
            LOGGER.error("An error occurred during the validation of the model " , e);
            return new ResponseEntity<>(metadataValidationResult, HttpStatus.INTERNAL_SERVER_ERROR);
        } finally {
            ModelUtils.closeQuietly(model);
            ModelUtils.closeQuietly(inferredModel);
            ModelUtils.closeQuietly(shapesModel);
            LOGGER.debug("Validation finished on endpoint '/validate' : {}.", metadataValidationResult);
            LOGGER.info("Validation finished on endpoint '/validate' : {} ms.", metadataValidationResult.getTotalTimeForValidationInMilliSeconds());
        }
    }

    @Operation(summary = "Infer the given Jena Model in RDF/JSON format and validate it with SHACL. " +
            "This endpoint is used for testing purposes of some SHACL shapes (if provided) against the model.")
    @PostMapping(value = "/test")
    public ResponseEntity<TestingMetadataValidationResult> validate(@RequestBody ValidateTestContext validateTestContext) {
        UUID uuid = UUID.randomUUID();
        LOGGER.info("Validation started on endpoint '/validate/test' for request id : {}.", uuid);
        TestingMetadataValidationResult testingMetadataValidationResult = new TestingMetadataValidationResult(uuid);
        boolean mustBeInferred = validateTestContext.isInfer();
        testingMetadataValidationResult.setMustBeInferred(mustBeInferred);
        boolean validateShapes = validateTestContext.isValidateShapes();
        testingMetadataValidationResult.setValidateShapes(validateShapes);

        Model model = null;
        Model shapesModel = null;
        Model inferredModel = null;
        try {
            model = processModelArgument(validateTestContext.getModel(), testingMetadataValidationResult);
            shapesModel = processShapesArgument(validateTestContext.getShapes(), testingMetadataValidationResult);

            if (mustBeInferred) {
                inferredModel = inferModel(model, testingMetadataValidationResult);
                validateModel(inferredModel, shapesModel, validateShapes, testingMetadataValidationResult);
            } else {
                validateModel(model, shapesModel, validateShapes, testingMetadataValidationResult);
            }
            return ResponseEntity.ok(testingMetadataValidationResult);
        } catch (BadRequestException bre) {
            testingMetadataValidationResult.setErrorMessage(bre.getMessage());
            return ResponseEntity.badRequest().body(testingMetadataValidationResult);
        } catch (Exception e) {
            testingMetadataValidationResult.setErrorMessage(e.getMessage());
            return new ResponseEntity<>(testingMetadataValidationResult, HttpStatus.INTERNAL_SERVER_ERROR);
        } finally {
            ModelUtils.closeQuietly(model);
            ModelUtils.closeQuietly(shapesModel);
            ModelUtils.closeQuietly(inferredModel);
            LOGGER.debug("Validation finished on endpoint '/validate/test' : {}.", testingMetadataValidationResult);
            LOGGER.info("Validation finished on endpoint '/validate/test' : {} ms.", testingMetadataValidationResult.getTotalTimeForValidationInMilliSeconds());
        }
    }

    private Model inferModel(Model model, MetadataValidationResult metadataValidationResult) {
        long startInferenceTime = System.currentTimeMillis();
        model = inferenceService.infer(model);
        long stopInferenceTime = System.currentTimeMillis();
        metadataValidationResult.setTotalTimeForInferenceInMilliSeconds((stopInferenceTime - startInferenceTime));
        return model;
    }

    private void validateModel(Model model, Model shapesModel, boolean validateShapes, MetadataValidationResult metadataValidationResult) {
        Model validationModel = null;
        try {
            long startValidationTime = System.currentTimeMillis();
            validationModel = validationService.validate(model, shapesModel, validateShapes);
            long stopValidationTime = System.currentTimeMillis();
            metadataValidationResult.setTotalTimeForValidationInMilliSeconds((stopValidationTime - startValidationTime));
            metadataValidationResult.setValidationResult(ModelUtils.writeModel(validationModel, Lang.RDFJSON));
            if (!validationService.modelPassedValidation(validationModel)) {
                metadataValidationResult.setErrorMessage("The model did not pass the SHACL validation. See response for the complete validation result");
            } else {
                metadataValidationResult.setConformModelProvided(true);
            }
        } finally {
            ModelUtils.closeQuietly(validationModel);
        }
    }

    private Model processShapesArgument(String shapesArg, TestingMetadataValidationResult validationResult)
            throws BadRequestException {
        if (StringUtils.isNotBlank(shapesArg)) {
            try {
                validationResult.setShapesArgProvided(true);
                Model shapes = ModelFactory.createDefaultModel().read(new StringReader(shapesArg), "", Lang.RDFJSON.getLabel());
                validationResult.setValidShapesProvided(true);
                return shapes;
            } catch (RiotException | JsonParseException e) {
                throw new BadRequestException("The shapes could not be parsed", e);
            }
        } else {
            return shapeService.getCopy();
        }
    }

    private Model processModelArgument(String modelStr, MetadataValidationResult metadataValidationResult) throws BadRequestException {
        if (StringUtils.isBlank(modelStr)) {
            throw new BadRequestException("The RDF/JSON 'model' argument cannot be empty.");
        } else {
            try {
                String normalizedModel = Normalizer.normalize(modelStr, Normalizer.Form.NFC);
                Model model = ModelFactory.createDefaultModel();
                RDFParser.create()
                        .fromString(normalizedModel)
                        .lang(Lang.RDFJSON)
                        .errorHandler(parserErrorHandler)
                        .parse(model);
                metadataValidationResult.setValidJenaModelProvided(true);
                return model;
            } catch (RiotException | JsonParseException e) {
                throw new BadRequestException("The format of the provided 'model' argument could not be parsed from RDF/JSON", e);
            }
        }
    }
}
