package eu.europa.ec.opoce.cellar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.retry.annotation.EnableRetry;


@EnableRetry
@SpringBootApplication
public class MetadataValidatorApplication {

    public static void main(String[] args) {
        SpringApplication.run(MetadataValidatorApplication.class, args);
    }
}
