package eu.europa.ec.opoce.cellar.service;

import org.apache.jena.rdf.model.Model;

public interface InferenceService extends ServiceStatusRetrievable {
    /**
     * This method returns a model ready to be validated by SHACL.
     * The inputModel is first normalized and combined to an inference model based on the CDM ontology.
     * Inference SPARQL queries defined in the configuration are then applied on this combined model.
     * Once this model is inferre, an extended model containing inScheme properties is added to the output model and returned.
     *
     * At the end of the process, the output model contains:
     * - the normalized triples of the inputModel
     * - its inferred triples from the inference model and the SPARQL queries (without all the triples of the ontology)
     * - the inScheme triples contained in the extended model
     *
     * @param inputModel a Jena Model; this model will not be modified nor closed
     * @return an normalized and inferred Jena Model containing inScheme properties
     */
    Model infer(Model inputModel);

    /**
     * This method goes through all statements of the input model and add them to the output model that is returned.
     * The statements are modified to use the CELLAR ID instead of another production identifier unless defined as sameAs.
     *
     * In other words, all the production identifiers (oj:ojId, celex:celexId, ...) will be replaced by their cellar:id
     * unless they are defining sameAs :
     * oj:ojId OWL.sameAs cellar:id
     * cellar:id OWL.sameAs oj:ojId
     *
     * @param inputModel a Jena Model; this model will not be modified nor closed
     * @return a normalized Jena Model
     */
    Model normalize(Model inputModel);
}
