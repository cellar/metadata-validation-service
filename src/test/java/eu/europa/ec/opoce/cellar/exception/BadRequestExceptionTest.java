package eu.europa.ec.opoce.cellar.exception;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BadRequestExceptionTest {

    @Test
    public void test() {
        final BadRequestException bre = new BadRequestException("message", new Exception("explanation"));
        assertEquals("message, reason: explanation", bre.getMessage());
    }

}