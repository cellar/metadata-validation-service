package eu.europa.ec.opoce.cellar.test;

import eu.europa.ec.opoce.cellar.configuration.FileConfiguration;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertNotNull;


@SpringBootTest(properties = "spring.profiles.active=arhs-dev")
public class MetadataValidatorServiceApplicationTest {

    @Autowired
    private FileConfiguration fileConfiguration;

    @Test
    public void contextLoads() {
        assertNotNull(fileConfiguration.getShapes());
        assertNotNull(fileConfiguration.getExtendedModel());
        assertNotNull(fileConfiguration.getInferenceModel());
    }
}
