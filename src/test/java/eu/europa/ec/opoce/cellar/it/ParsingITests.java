package eu.europa.ec.opoce.cellar.it;

import eu.europa.ec.opoce.cellar.IntegrationTest;
import eu.europa.ec.opoce.cellar.domain.MetadataValidationResult;
import eu.europa.ec.opoce.cellar.domain.ValidateTestContext;
import org.apache.jena.riot.Lang;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.ActiveProfiles;

import java.nio.file.Path;
import java.nio.file.Paths;

import static eu.europa.ec.opoce.cellar.it.Utils.loadPathInModelAndReturnJsonFormat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@IntegrationTest
@ActiveProfiles("it")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ParsingITests {

    private static final Logger LOGGER = LoggerFactory.getLogger(ParsingITests.class);

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void testCase_01() throws Exception {
        LOGGER.info("testCase_01 : model contains parsing errors -> returns the error");

        String modelRdfJson = loadPathInModelAndReturnJsonFormat("classpath:/eu/europa/ec/opoce/cellar/test/it/parsing/model.ttl", Lang.TTL);
        String shapesRdfJson = loadPathInModelAndReturnJsonFormat("classpath:/eu/europa/ec/opoce/cellar/test/it/parsing/shapes.ttl", Lang.TTL);

        ValidateTestContext validateTestContext = new ValidateTestContext();
        validateTestContext.setModel(modelRdfJson);
        validateTestContext.setShapes(shapesRdfJson);
        validateTestContext.setInfer(true);
        validateTestContext.setValidateShapes(true);

        MetadataValidationResult result = restTemplate.postForObject("/validate/test", validateTestContext, MetadataValidationResult.class);
        assertNotNull(result.getRequestId());
        assertEquals("The format of the provided 'model' argument could not be parsed from RDF/JSON, reason: "
                + "[line: 10, col: 17] Lexical form '' not valid for datatype XSD boolean", result.getErrorMessage());
        assertEquals("", result.getValidationResult());
        assertFalse(result.isValidJenaModelProvided());
        assertFalse(result.conformModelProvided());
        assertEquals(-1, result.getTotalTimeForInferenceInMilliSeconds());
        assertEquals(-1, result.getTotalTimeForValidationInMilliSeconds());
    }
}
