package eu.europa.ec.opoce.cellar.test.service;

import eu.europa.ec.opoce.cellar.TestUtils;
import eu.europa.ec.opoce.cellar.service.TopBraidShaclValidationService;
import eu.europa.ec.opoce.cellar.service.ValidationService;
import org.apache.jena.arq.querybuilder.AskBuilder;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.vocabulary.OWL;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TopBraidShaclValidationServiceTest {

    private static ValidationService validationService;

    @BeforeAll
    public static void oneTimeSetUp() throws Exception {
        validationService = new TopBraidShaclValidationService();
    }

    @Test
    public void inputModelCannotBeNull() throws Exception {
        Assertions.assertThrows(IllegalArgumentException.class, () -> validationService.validate(null, ModelFactory.createDefaultModel(), false));
    }

    @Test
    public void inputShapesModelCannotBeNull() throws Exception {
        Assertions.assertThrows(IllegalArgumentException.class, () -> validationService.validate(ModelFactory.createDefaultModel(), null, false));
    }

    @Test
    public void emptyModels_validate_confirmTrue() throws Exception {
        Model validationResult = validationService.validate(ModelFactory.createDefaultModel(),
                ModelFactory.createDefaultModel(), false);
        assertTrue(isValid(validationResult));
        assertNotNull(validationResult);
    }

    private boolean isValid(Model validationResult) {
        AskBuilder askBuilder = new AskBuilder();
        askBuilder.addWhere("?s", "<http://www.w3.org/ns/shacl#conforms>", "\"true\"^^<http://www.w3.org/2001/XMLSchema#boolean>");
        Query query = askBuilder.build();
        QueryExecution queryExecution = QueryExecutionFactory.create(query, validationResult);
        return queryExecution.execAsk();
    }

    @Test
    public void addSameAsOfFocusNodeTest() throws Exception {

        String prefix = "http://datashapes.org/sh/tests/core/property/datatype-001.test#";
        Model model = TestUtils.getModel("classpath:/eu/europa/ec/opoce/cellar/test/validation.impl/model.ttl");
        Model shapes = TestUtils.getModel("classpath:/eu/europa/ec/opoce/cellar/test/validation.impl/shapes.ttl");
        Model validationModel = validationService.validate(model, shapes, false);

        Resource resource1 = ResourceFactory.createResource(prefix + "InvalidResource1");
        Resource resource2 = ResourceFactory.createResource(prefix + "InvalidResource2");
        Resource resource3 = ResourceFactory.createResource(prefix + "InvalidResource3");

        assertTrue(validationModel.contains(resource1, OWL.sameAs, prefix + "AAA"));
        assertTrue(validationModel.contains(resource1, OWL.sameAs, prefix + "BBB"));
        assertTrue(validationModel.contains(resource1, OWL.sameAs, prefix + "CCC"));
        assertTrue(validationModel.contains(resource2, OWL.sameAs, prefix + "DDD"));
        assertFalse(validationModel.contains(resource3, OWL.sameAs));
    }
}