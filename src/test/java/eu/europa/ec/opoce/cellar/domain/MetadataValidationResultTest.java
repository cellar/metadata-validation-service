/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.domain
 *             FILE : MetadataValidationResultTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 12 13, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-12-13 07:56:34 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.domain;

import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

/**
 * @author ARHS Developments
 */
public class MetadataValidationResultTest {

    @Test
    public void defaultToStringOutput() throws Exception {
        UUID uuid = UUID.randomUUID();
        MetadataValidationResult mvr = new MetadataValidationResult(uuid);
        assertEquals("MetadataValidationResult{requestId=" + uuid.toString() + ", errorMessage='', " +
                "validationResult='', validJenaModelProvided=false, conformModelProvided=false, totalTimeForInferenceInMilliSeconds=-1, " +
                "totalTimeForValidationInMilliSeconds=-1}", mvr.toString());
    }

    @Test
    public void completeValuesToStringOutput() throws Exception {
        UUID uuid = UUID.randomUUID();
        MetadataValidationResult mvr = new MetadataValidationResult(uuid);
        mvr.setErrorMessage("errorMessage");
        mvr.setValidationResult("{ }");
        mvr.setValidJenaModelProvided(true);
        mvr.setConformModelProvided(false);
        mvr.setTotalTimeForInferenceInMilliSeconds(50);
        mvr.setTotalTimeForValidationInMilliSeconds(100);
        assertEquals("MetadataValidationResult{requestId=" + uuid.toString() + ", errorMessage='errorMessage', " +
                "validationResult='{ }', validJenaModelProvided=true, conformModelProvided=false, totalTimeForInferenceInMilliSeconds=50, " +
                "totalTimeForValidationInMilliSeconds=100}", mvr.toString());
    }

    @Test
    public void allFieldsMustBeEqual() throws Exception {
        UUID uuidOne = UUID.randomUUID();
        MetadataValidationResult mvrOne = new MetadataValidationResult(uuidOne);
        mvrOne.setErrorMessage("errorMessage");
        mvrOne.setValidationResult("{ }");
        mvrOne.setValidJenaModelProvided(true);
        mvrOne.setConformModelProvided(false);
        mvrOne.setTotalTimeForInferenceInMilliSeconds(50);
        mvrOne.setTotalTimeForValidationInMilliSeconds(100);

        UUID uuidTwo = UUID.randomUUID();
        MetadataValidationResult mvrTwo = new MetadataValidationResult(uuidTwo);
        mvrTwo.setErrorMessage("errorMessage");
        mvrTwo.setValidationResult("{ }");
        mvrTwo.setValidJenaModelProvided(true);
        mvrTwo.setConformModelProvided(false);
        mvrTwo.setTotalTimeForInferenceInMilliSeconds(50);
        mvrTwo.setTotalTimeForValidationInMilliSeconds(100);

        assertNotEquals(mvrOne, mvrTwo);
        assertNotEquals(mvrOne.hashCode(), mvrTwo.hashCode());
        mvrTwo.setRequestId(uuidOne);
        assertEquals(mvrOne, mvrTwo);
        assertEquals(mvrOne.hashCode(), mvrTwo.hashCode());
    }
}