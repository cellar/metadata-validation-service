/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.domain
 *             FILE : TestingMetadataValidationResultTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 12 13, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-12-13 08:02:00 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.domain;


import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;


/**
 * @author ARHS Developments
 */
public class TestingMetadataValidationResultTest {

    @Test
    public void defaultToStringOutputContainsParentMetadataValidationResultField() throws Exception {
        UUID uuid = UUID.randomUUID();
        TestingMetadataValidationResult tmvr = new TestingMetadataValidationResult(uuid);
        assertEquals("TestingMetadataValidationResult{MetadataValidationResult{requestId=" + uuid.toString() + ", " +
                "errorMessage='', validationResult='', validJenaModelProvided=false, conformModelProvided=false, totalTimeForInferenceInMilliSeconds=-1, " +
                "totalTimeForValidationInMilliSeconds=-1}shapesArgProvided=false, validShapesProvided=false, validateShapes=false, mustBeInferred=false}",
                tmvr.toString());
    }

    @Test
    public void completeValuesToStringOutputContainsParentMetadataValidationResultField() throws Exception {
        UUID uuid = UUID.randomUUID();
        TestingMetadataValidationResult tmvr = new TestingMetadataValidationResult(uuid);
        tmvr.setShapesArgProvided(true);
        tmvr.setValidateShapes(true);
        tmvr.setValidShapesProvided(true);
        tmvr.setMustBeInferred(true);
        tmvr.setErrorMessage("errorMessage");
        tmvr.setValidationResult("{ }");
        tmvr.setValidJenaModelProvided(true);
        tmvr.setConformModelProvided(false);
        tmvr.setTotalTimeForInferenceInMilliSeconds(50);
        tmvr.setTotalTimeForValidationInMilliSeconds(100);
        assertEquals("TestingMetadataValidationResult{MetadataValidationResult{requestId=" + uuid.toString() + ", " +
                "errorMessage='errorMessage', validationResult='{ }', validJenaModelProvided=true, conformModelProvided=false, " +
                "totalTimeForInferenceInMilliSeconds=50, totalTimeForValidationInMilliSeconds=100}shapesArgProvided=true, validShapesProvided=true, " +
                "validateShapes=true, mustBeInferred=true}", tmvr.toString());
    }
    
    @Test
    public void allFieldsMustBeEqual() throws Exception {
        UUID uuidOne = UUID.randomUUID();
        TestingMetadataValidationResult tmvrOne = new TestingMetadataValidationResult(uuidOne);
        tmvrOne.setShapesArgProvided(true);
        tmvrOne.setValidateShapes(true);
        tmvrOne.setValidShapesProvided(true);
        tmvrOne.setMustBeInferred(true);
        tmvrOne.setErrorMessage("errorMessage");
        tmvrOne.setValidationResult("{ }");
        tmvrOne.setValidJenaModelProvided(true);
        tmvrOne.setConformModelProvided(false);
        tmvrOne.setTotalTimeForInferenceInMilliSeconds(50);
        tmvrOne.setTotalTimeForValidationInMilliSeconds(100);

        UUID uuidTwo = UUID.randomUUID();
        TestingMetadataValidationResult tmvrTwo = new TestingMetadataValidationResult(uuidTwo);
        tmvrTwo.setShapesArgProvided(true);
        tmvrTwo.setValidateShapes(true);
        tmvrTwo.setValidShapesProvided(true);
        tmvrTwo.setMustBeInferred(true);
        tmvrTwo.setErrorMessage("errorMessage");
        tmvrTwo.setValidationResult("{ }");
        tmvrTwo.setValidJenaModelProvided(true);
        tmvrTwo.setConformModelProvided(false);
        tmvrTwo.setTotalTimeForInferenceInMilliSeconds(50);
        tmvrTwo.setTotalTimeForValidationInMilliSeconds(100);

        assertNotEquals(tmvrOne, tmvrTwo);
        assertNotEquals(tmvrOne.hashCode(), tmvrTwo.hashCode());
        tmvrTwo.setRequestId(uuidOne);
        assertEquals(tmvrOne, tmvrTwo);
        assertEquals(tmvrOne.hashCode(), tmvrTwo.hashCode());
    }
}