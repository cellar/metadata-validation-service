package eu.europa.ec.opoce.cellar.it;

import eu.europa.ec.opoce.cellar.IntegrationTest;
import eu.europa.ec.opoce.cellar.TestUtils;
import eu.europa.ec.opoce.cellar.domain.MetadataValidationResult;
import eu.europa.ec.opoce.cellar.domain.ValidateContext;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.Lang;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.ActiveProfiles;

import java.io.StringReader;
import java.io.StringWriter;

import static eu.europa.ec.opoce.cellar.it.Utils.loadPathInModelAndReturnJsonFormat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@IntegrationTest
@ActiveProfiles("it")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ValidationITests {

    private static final Logger LOGGER = LoggerFactory.getLogger(ValidationITests.class);

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void testCase_01() throws Exception {
        LOGGER.info("testCase_01 : model is valid -> validation returns true");

        String validModelJson = loadPathInModelAndReturnJsonFormat("classpath:/eu/europa/ec/opoce/cellar/test/it/models/valid_model.ttl", Lang.TURTLE);

        ValidateContext validateContext = new ValidateContext();
        validateContext.setModel(validModelJson);
        MetadataValidationResult result = restTemplate.postForObject("/validate", validateContext, MetadataValidationResult.class);
        assertNotNull(result.getRequestId());
        assertNotNull(result.getErrorMessage());
        assertNotNull(result.getValidationResult());
        assertTrue(result.isValidJenaModelProvided());
        assertTrue(result.conformModelProvided());
        assertNotEquals(-1, result.getTotalTimeForInferenceInMilliSeconds());
        assertNotEquals(-1, result.getTotalTimeForValidationInMilliSeconds());
    }

    @Test
    public void testCase_02() throws Exception {
        LOGGER.info("testCase_02 : model is invalid -> validation returns false");
        String invalidModelJson = loadPathInModelAndReturnJsonFormat("classpath:/eu/europa/ec/opoce/cellar/test/it/models/invalid_model.ttl", Lang.TURTLE);

        ValidateContext validateContext = new ValidateContext();
        validateContext.setModel(invalidModelJson);
        MetadataValidationResult result = restTemplate.postForObject("/validate", validateContext, MetadataValidationResult.class);

        assertNotNull(result.getRequestId());
        assertNotNull(result.getErrorMessage());
        assertNotNull(result.getValidationResult());
        assertTrue(result.isValidJenaModelProvided());
        assertFalse(result.conformModelProvided());
        assertNotEquals(-1, result.getTotalTimeForInferenceInMilliSeconds());
        assertNotEquals(-1, result.getTotalTimeForValidationInMilliSeconds());

        Model validationResultModel = ModelFactory.createDefaultModel()
                .read(new StringReader(result.getValidationResult()), "", Lang.RDFJSON.getLabel());

        String query = QueryFactory.create("" +
                "ASK\n" +
                "WHERE\n" +
                "   { ?s  <http://www.w3.org/ns/shacl#conforms>  false ;\n" +
                "         <http://www.w3.org/ns/shacl#result>  ?o .\n" +
                "     ?o  <http://www.w3.org/ns/shacl#resultPath>  <http://publications.europa.eu/ontology/cdm#resource_legal_number_natural_celex> ;\n" +
                "         <http://www.w3.org/ns/shacl#value>  \"abc\"^^<http://www.w3.org/2001/XMLSchema#positiveInteger> ;\n" +
                "         <http://www.w3.org/ns/shacl#resultMessage>  \"Value does not have datatype http://www.w3.org/2001/XMLSchema#positiveInteger\" ;\n" +
                "         <http://www.w3.org/ns/shacl#resultSeverity>  <http://www.w3.org/ns/shacl#Violation>\n" +
                "   }").toString();
        QueryExecution queryExecution = QueryExecutionFactory.create(query, validationResultModel);
        assertTrue(queryExecution.execAsk());
    }

    @Test
    public void testCase_03() throws Exception {
        LOGGER.info("testCase_03 : model is valid -> validation returns true");

        String validModelJson = loadPathInModelAndReturnJsonFormat("classpath:/eu/europa/ec/opoce/cellar/test/it/models/valid_greek_model.rdf", Lang.RDFXML);

        ValidateContext validateContext = new ValidateContext();
        validateContext.setModel(validModelJson);
        MetadataValidationResult result = restTemplate.postForObject("/validate", validateContext, MetadataValidationResult.class);
        assertNotNull(result.getRequestId());
        assertNotNull(result.getErrorMessage());
        assertNotNull(result.getValidationResult());
        assertTrue(result.isValidJenaModelProvided());
        assertTrue(result.conformModelProvided());
        assertNotEquals(-1, result.getTotalTimeForInferenceInMilliSeconds());
        assertNotEquals(-1, result.getTotalTimeForValidationInMilliSeconds());
    }

    @Test
    public void realTestCaseScenarioForShapesDebugging() throws Exception {
        // get the metadata in RDF/XML format and create a Jena Model
        String content = TestUtils.getModelAsString("classpath:/eu/europa/ec/opoce/cellar/test/it/real_scenario/mets_metadata.ttl");
        Model model = ModelFactory.createDefaultModel().read(new StringReader(content), "", Lang.TURTLE.getLabel());

        // store the model in RDF/JSON format for the REST endpoint
        StringWriter stringWriter = new StringWriter();
        model.write(stringWriter, Lang.RDFJSON.getLabel());
        String modelAsRdfJson = stringWriter.toString();

        // send it to the service and get the result
        ValidateContext validateContext = new ValidateContext();
        validateContext.setModel(modelAsRdfJson);
        MetadataValidationResult result = restTemplate.postForObject("/validate", validateContext, MetadataValidationResult.class);

        // print the validation result in the console
        System.out.println(result.getValidationResult());
    }
}
