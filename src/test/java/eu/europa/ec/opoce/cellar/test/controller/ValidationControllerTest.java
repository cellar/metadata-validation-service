package eu.europa.ec.opoce.cellar.test.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.europa.ec.opoce.cellar.controller.ValidationController;
import eu.europa.ec.opoce.cellar.domain.MetadataValidationResult;
import eu.europa.ec.opoce.cellar.domain.TestingMetadataValidationResult;
import eu.europa.ec.opoce.cellar.domain.ValidateContext;
import eu.europa.ec.opoce.cellar.domain.ValidateTestContext;
import eu.europa.ec.opoce.cellar.exception.ParserErrorHandler;
import eu.europa.ec.opoce.cellar.service.InferenceService;
import eu.europa.ec.opoce.cellar.service.ModelFileExtensionService;
import eu.europa.ec.opoce.cellar.service.ShapeService;
import eu.europa.ec.opoce.cellar.service.ValidationService;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.Lang;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.StringReader;
import java.io.StringWriter;

import static org.hamcrest.Matchers.greaterThan;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.Mockito.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(ValidationController.class)
public class ValidationControllerTest {
    private static String EMPTY_MODEL_RDFJSON;
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private ValidationService validationService;
    @MockBean
    private ShapeService shapeService;
    @MockBean
    private ModelFileExtensionService modelFileExtensionService;
    @MockBean
    private InferenceService inferenceService;
    @MockBean
    private ParserErrorHandler parserErrorHandler;

    @BeforeAll
    public static void oneTimeSetup() throws Exception {
        Model emptyModel = ModelFactory.createDefaultModel();
        StringWriter stringWriter = new StringWriter();
        emptyModel.write(stringWriter, Lang.RDFJSON.getLabel());
        EMPTY_MODEL_RDFJSON = stringWriter.toString();
    }

    @Test
    public void jsonReturnedFromValidateEndpointIsOfTypeValidationResult() throws Exception {
        when(validationService.validate(any(), any(), anyBoolean())).thenReturn(ModelFactory.createDefaultModel());
        when(validationService.modelPassedValidation(any())).thenReturn(true);
        when(shapeService.getCopy()).thenReturn(ModelFactory.createDefaultModel());
        when(inferenceService.infer(any())).thenReturn(ModelFactory.createDefaultModel());

        ValidateContext validateContext = new ValidateContext();
        validateContext.setModel(EMPTY_MODEL_RDFJSON);

        MvcResult mvcResult = mockMvc.perform(post("/validate")
                .contentType(APPLICATION_JSON_UTF8)
                .content(asJsonString(validateContext)))
                .andExpect(status().is(HttpStatus.OK.value()))
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$.errorMessage").isEmpty())
                .andExpect(jsonPath("$.validationResult").value("{ }"))
                .andExpect(jsonPath("$.validJenaModelProvided").value(true))
                .andExpect(jsonPath("$.conformModelProvided").value(true))
                .andExpect(jsonPath("$.requestId").isNotEmpty())
                .andReturn();

        verify(shapeService, times(1)).getCopy();
        verify(inferenceService, times(1)).infer(any());
        verify(validationService, times(1)).validate(any(), any(), anyBoolean());
        verify(validationService, times(1)).modelPassedValidation(any());

        // check that the object jsonified is of type MetadataValidationResult and not TestingMetadataValidationResult
        String content = mvcResult.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        MetadataValidationResult metadataValidationResult = mapper.readValue(content, MetadataValidationResult.class);
        assertNotNull(metadataValidationResult);
    }

    private String asJsonString(Object object) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(object);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void noModelProvidedThenHttpBadRequest() throws Exception {
        mockMvc.perform(post("/validate").content(asJsonString(new ValidateContext())).contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$.errorMessage").value("The RDF/JSON 'model' argument cannot be empty."))
                .andExpect(jsonPath("$.validJenaModelProvided").value(false))
                .andExpect(jsonPath("$.conformModelProvided").value(false))
                .andExpect(jsonPath("$.totalTimeForInferenceInMilliSeconds").value(-1))
                .andExpect(jsonPath("$.totalTimeForValidationInMilliSeconds").value(-1))
                .andExpect(jsonPath("$.validationResult").isEmpty())
                .andExpect(jsonPath("$.requestId").isNotEmpty())
                .andReturn();

        verify(shapeService, times(0)).getCopy();
        verify(inferenceService, times(0)).infer(any());
        verify(validationService, times(0)).validate(any(), any(), anyBoolean());
    }

    @Test
    public void invalidModelFormatThenHttpBadRequest() throws Exception {
        String turtleModel = "" +
                "@prefix ex: <http://example.com/ns#> .\n" +
                "ex:Alice a ex:Person .\n";

        ValidateContext validateContext = new ValidateContext();
        validateContext.setModel(turtleModel);

        mockMvc.perform(post("/validate").content(asJsonString(validateContext)).contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$.validJenaModelProvided").value(false))
                .andExpect(jsonPath("$.conformModelProvided").value(false))
                .andExpect(jsonPath("$.errorMessage").value("The format of the provided 'model' argument could not be parsed from RDF/JSON, reason: Unknown char: @(64)"))
                .andExpect(jsonPath("$.requestId").isNotEmpty());

        verify(shapeService, times(0)).getCopy();
        verify(inferenceService, times(0)).infer(any());
        verify(validationService, times(0)).validate(any(), any(), anyBoolean());
    }

    @Test
    public void validModelArgShapesLoadedOntologyLoadedThenReturnHttpOk() throws Exception {
        when(validationService.validate(any(), any(), anyBoolean())).thenReturn(ModelFactory.createDefaultModel());
        when(validationService.modelPassedValidation(any())).thenReturn(true);
        when(shapeService.getCopy()).thenReturn(ModelFactory.createDefaultModel());
        when(inferenceService.infer(any())).thenReturn(ModelFactory.createDefaultModel());

        ValidateContext validateContext = new ValidateContext();
        validateContext.setModel(EMPTY_MODEL_RDFJSON);

        mockMvc.perform(post("/validate").content(asJsonString(validateContext)).contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().is(HttpStatus.OK.value()))
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$.validJenaModelProvided").value(true))
                .andExpect(jsonPath("$.errorMessage").isEmpty())
                .andExpect(jsonPath("$.totalTimeForInferenceInMilliSeconds").value(greaterThan(-1)))
                .andExpect(jsonPath("$.totalTimeForValidationInMilliSeconds").value(greaterThan(-1)))
                .andExpect(jsonPath("$.validationResult").isNotEmpty())
                .andExpect(jsonPath("$.conformModelProvided").value(true))
                .andExpect(jsonPath("$.requestId").isNotEmpty());

        verify(shapeService, times(1)).getCopy();
        verify(inferenceService, times(1)).infer(any());
        verify(validationService, times(1)).validate(any(), any(), anyBoolean());
        verify(validationService, times(1)).modelPassedValidation(any());
    }

    @Test
    public void jsonReturnedFromValidateTestEndpointIsOfTypeTestingValidationResult() throws Exception {
        when(validationService.validate(any(), any(), anyBoolean())).thenReturn(ModelFactory.createDefaultModel());
        when(validationService.modelPassedValidation(any())).thenReturn(true);
        when(shapeService.getCopy()).thenReturn(ModelFactory.createDefaultModel());

        ValidateTestContext validateTestContext = new ValidateTestContext();
        validateTestContext.setModel(EMPTY_MODEL_RDFJSON);

        MvcResult mvcResult = mockMvc.perform(post("/validate/test")
                .contentType(APPLICATION_JSON_UTF8)
                .content(asJsonString(validateTestContext)))
                .andExpect(status().is(HttpStatus.OK.value()))
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$.errorMessage").isEmpty())
                .andExpect(jsonPath("$.validationResult").value("{ }"))
                .andExpect(jsonPath("$.validJenaModelProvided").value(true))
                .andExpect(jsonPath("$.mustBeInferred").value(true))
                .andExpect(jsonPath("$.shapesArgProvided").value(false))
                .andExpect(jsonPath("$.conformModelProvided").value(true))
                .andExpect(jsonPath("$.requestId").isNotEmpty())
                .andReturn();

        verify(shapeService, times(1)).getCopy();
        verify(inferenceService, times(1)).infer(any());
        verify(validationService, times(1)).validate(any(), any(), anyBoolean());
        verify(validationService, times(1)).modelPassedValidation(any());
        // check that the object jsonified is of type TestingMetadataValidationResult and not MetadataValidationResult
        String content = mvcResult.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        TestingMetadataValidationResult testingValidationResult = mapper.readValue(content, TestingMetadataValidationResult.class);
        assertNotNull(testingValidationResult);
    }

    @Test
    public void noArgumentsProvidedForTestingEndpointThenHttpBadRequest() throws Exception {
        mockMvc.perform(post("/validate/test")
                .contentType(APPLICATION_JSON_UTF8)
                .content(asJsonString(new ValidateTestContext())))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$.mustBeInferred").value(true))
                .andExpect(jsonPath("$.errorMessage").value("The RDF/JSON 'model' argument cannot be empty."))
                .andExpect(jsonPath("$.validJenaModelProvided").value(false))
                .andExpect(jsonPath("$.shapesArgProvided").value(false))
                .andExpect(jsonPath("$.conformModelProvided").value(false))
                .andExpect(jsonPath("$.requestId").isNotEmpty());

        verify(shapeService, times(0)).getCopy();
        verify(inferenceService, times(0)).infer(any());
        verify(validationService, times(0)).validate(any(), any(), anyBoolean());
    }

    @Test
    public void invalidJenaModelProvidedForTestingEndpointThenHttpBadRequest() throws Exception {
        String turtleModel = "" +
                "@prefix ex: <http://example.com/ns#> .\n" +
                "ex:Alice a ex:Person .\n";

        ValidateTestContext validateTestContext = new ValidateTestContext();
        validateTestContext.setModel(turtleModel);

        mockMvc.perform(post("/validate/test").content(asJsonString(validateTestContext)).contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$.mustBeInferred").value(true))
                .andExpect(jsonPath("$.errorMessage").value("The format of the provided 'model' argument could not be parsed from RDF/JSON, reason: Unknown char: @(64)"))
                .andExpect(jsonPath("$.validJenaModelProvided").value(false))
                .andExpect(jsonPath("$.shapesArgProvided").value(false))
                .andExpect(jsonPath("$.conformModelProvided").value(false))
                .andExpect(jsonPath("$.requestId").isNotEmpty());

        verify(shapeService, times(0)).getCopy();
        verify(inferenceService, times(0)).infer(any());
        verify(validationService, times(0)).validate(any(), any(), anyBoolean());
    }

    @Test
    public void validModelInferenceArgNotProvidedShapesNotProvidedShapesLoadedValidateShapeIsTrueThenHttpOk() throws Exception {
        when(validationService.validate(any(), any(), anyBoolean())).thenReturn(ModelFactory.createDefaultModel());
        when(validationService.modelPassedValidation(any())).thenReturn(true);
        when(shapeService.getCopy()).thenReturn(ModelFactory.createDefaultModel());
        when(inferenceService.infer(any())).thenReturn(ModelFactory.createDefaultModel());

        ValidateTestContext validateTestContext = new ValidateTestContext();
        validateTestContext.setModel(EMPTY_MODEL_RDFJSON);
        validateTestContext.setValidateShapes(true);

        mockMvc.perform(post("/validate/test").content(asJsonString(validateTestContext)).contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().is(HttpStatus.OK.value()))
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$.errorMessage").isEmpty())
                .andExpect(jsonPath("$.validationResult").value("{ }"))
                .andExpect(jsonPath("$.validJenaModelProvided").value(true))
                .andExpect(jsonPath("$.mustBeInferred").value(true))
                .andExpect(jsonPath("$.shapesArgProvided").value(false))
                .andExpect(jsonPath("$.validateShapes").value(true))
                .andExpect(jsonPath("$.conformModelProvided").value(true))
                .andExpect(jsonPath("$.requestId").isNotEmpty());

        verify(shapeService, times(1)).getCopy();
        verify(inferenceService, times(1)).infer(any());
        verify(validationService, times(1)).validate(any(), any(), anyBoolean());
        verify(validationService, times(1)).modelPassedValidation(any());
    }

    @Test
    public void validModelInferenceArgNotProvidedShapesNotProvidedShapesLoadedValidateShapeIsFalseThenHttpOk() throws Exception {
        when(validationService.validate(any(), any(), anyBoolean())).thenReturn(ModelFactory.createDefaultModel());
        when(validationService.modelPassedValidation(any())).thenReturn(true);
        when(shapeService.getCopy()).thenReturn(ModelFactory.createDefaultModel());
        when(inferenceService.infer(any())).thenReturn(ModelFactory.createDefaultModel());

        ValidateTestContext validateTestContext = new ValidateTestContext();
        validateTestContext.setModel(EMPTY_MODEL_RDFJSON);
        validateTestContext.setValidateShapes(false);

        mockMvc.perform(post("/validate/test").content(asJsonString(validateTestContext)).contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().is(HttpStatus.OK.value()))
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$.errorMessage").isEmpty())
                .andExpect(jsonPath("$.validationResult").value("{ }"))
                .andExpect(jsonPath("$.validJenaModelProvided").value(true))
                .andExpect(jsonPath("$.mustBeInferred").value(true))
                .andExpect(jsonPath("$.shapesArgProvided").value(false))
                .andExpect(jsonPath("$.validateShapes").value(false))
                .andExpect(jsonPath("$.conformModelProvided").value(true))
                .andExpect(jsonPath("$.requestId").isNotEmpty());

        verify(shapeService, times(1)).getCopy();
        verify(inferenceService, times(1)).infer(any());
        verify(validationService, times(1)).validate(any(), any(), anyBoolean());
        verify(validationService, times(1)).modelPassedValidation(any());
    }

    @Test
    public void validModelInferenceArgNotProvidedValidShapesProvidedValidateShapesTrueThenReturnHttpOk() throws Exception {
        when(validationService.validate(any(), any(), anyBoolean())).thenReturn(ModelFactory.createDefaultModel());
        when(validationService.modelPassedValidation(any())).thenReturn(true);
        when(shapeService.getCopy()).thenReturn(ModelFactory.createDefaultModel());
        when(inferenceService.infer(any())).thenReturn(ModelFactory.createDefaultModel());

        ValidateTestContext validateTestContext = new ValidateTestContext();
        validateTestContext.setModel(EMPTY_MODEL_RDFJSON);
        validateTestContext.setShapes(EMPTY_MODEL_RDFJSON);
        validateTestContext.setValidateShapes(true);

        mockMvc.perform(post("/validate/test").content(asJsonString(validateTestContext)).contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().is(HttpStatus.OK.value()))
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$.errorMessage").isEmpty())
                .andExpect(jsonPath("$.validationResult").value("{ }"))
                .andExpect(jsonPath("$.validJenaModelProvided").value(true))
                .andExpect(jsonPath("$.mustBeInferred").value(true))
                .andExpect(jsonPath("$.shapesArgProvided").value(true))
                .andExpect(jsonPath("$.validShapesProvided").value(true))
                .andExpect(jsonPath("$.validateShapes").value(true))
                .andExpect(jsonPath("$.conformModelProvided").value(true))
                .andExpect(jsonPath("$.requestId").isNotEmpty());

        verify(shapeService, times(0)).getCopy();
        verify(inferenceService, times(1)).infer(any());
        verify(validationService, times(1)).validate(any(), any(), anyBoolean());
        verify(validationService, times(1)).modelPassedValidation(any());
    }

    @Test
    public void validModelInferenceArgNotProvidedValidShapesProvidedValidateShapesFalseThenReturnHttpOk() throws Exception {
        when(validationService.validate(any(), any(), anyBoolean())).thenReturn(ModelFactory.createDefaultModel());
        when(validationService.modelPassedValidation(any())).thenReturn(true);
        when(shapeService.getCopy()).thenReturn(ModelFactory.createDefaultModel());
        when(inferenceService.infer(any())).thenReturn(ModelFactory.createDefaultModel());

        ValidateTestContext validateTestContext = new ValidateTestContext();
        validateTestContext.setModel(EMPTY_MODEL_RDFJSON);
        validateTestContext.setShapes(EMPTY_MODEL_RDFJSON);
        validateTestContext.setValidateShapes(false);

        mockMvc.perform(post("/validate/test").content(asJsonString(validateTestContext)).contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().is(HttpStatus.OK.value()))
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$.errorMessage").isEmpty())
                .andExpect(jsonPath("$.validationResult").value("{ }"))
                .andExpect(jsonPath("$.validJenaModelProvided").value(true))
                .andExpect(jsonPath("$.mustBeInferred").value(true))
                .andExpect(jsonPath("$.shapesArgProvided").value(true))
                .andExpect(jsonPath("$.validShapesProvided").value(true))
                .andExpect(jsonPath("$.validateShapes").value(false))
                .andExpect(jsonPath("$.conformModelProvided").value(true))
                .andExpect(jsonPath("$.requestId").isNotEmpty());

        verify(shapeService, times(0)).getCopy();
        verify(inferenceService, times(1)).infer(any());
        verify(validationService, times(1)).validate(any(), any(), anyBoolean());
        verify(validationService, times(1)).modelPassedValidation(any());
    }

    @Test
    public void validModelInferenceArgNotProvidedInvalidShapesProvidedThenReturnHttpBadRequest() throws Exception {
        when(validationService.validate(any(), any(), anyBoolean())).thenReturn(ModelFactory.createDefaultModel());
        when(inferenceService.infer(any())).thenReturn(ModelFactory.createDefaultModel());

        String turtleModel = "" +
                "@prefix ex: <http://example.com/ns#> .\n" +
                "ex:Alice a ex:Person .\n";

        ValidateTestContext validateTestContext = new ValidateTestContext();
        validateTestContext.setModel(EMPTY_MODEL_RDFJSON);
        validateTestContext.setShapes(turtleModel);

        mockMvc.perform(post("/validate/test").content(asJsonString(validateTestContext)).contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$.errorMessage").value(Matchers.startsWith("The shapes could not be parsed, reason")))
                .andExpect(jsonPath("$.validJenaModelProvided").value(true))
                .andExpect(jsonPath("$.mustBeInferred").value(true))
                .andExpect(jsonPath("$.shapesArgProvided").value(true))
                .andExpect(jsonPath("$.validShapesProvided").value(false))
                .andExpect(jsonPath("$.conformModelProvided").value(false))
                .andExpect(jsonPath("$.requestId").isNotEmpty());

        verify(shapeService, times(0)).getCopy();
        verify(inferenceService, times(0)).infer(any());
        verify(validationService, times(0)).validate(any(), any(), anyBoolean());
    }

    @Test
    public void validModelInferenceArgProvidedShapesNotProvidedShapesLoadedOntologyLoadedThenReturnHttpOk() throws Exception {
        when(validationService.validate(any(), any(), anyBoolean())).thenReturn(ModelFactory.createDefaultModel());
        when(validationService.modelPassedValidation(any())).thenReturn(true);
        when(shapeService.getCopy()).thenReturn(ModelFactory.createDefaultModel());
        when(inferenceService.infer(any())).thenReturn(ModelFactory.createDefaultModel());

        ValidateTestContext validateTestContext = new ValidateTestContext();
        validateTestContext.setModel(EMPTY_MODEL_RDFJSON);
        validateTestContext.setInfer(false);

        mockMvc.perform(post("/validate/test").content(asJsonString(validateTestContext)).contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().is(HttpStatus.OK.value()))
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$.errorMessage").isEmpty())
                .andExpect(jsonPath("$.validJenaModelProvided").value(true))
                .andExpect(jsonPath("$.mustBeInferred").value(false))
                .andExpect(jsonPath("$.shapesArgProvided").value(false))
                .andExpect(jsonPath("$.conformModelProvided").value(true))
                .andExpect(jsonPath("$.totalTimeForInferenceInMilliSeconds").value(-1))
                .andExpect(jsonPath("$.totalTimeForValidationInMilliSeconds").value(greaterThan(-1)))
                .andExpect(jsonPath("$.requestId").isNotEmpty());

        verify(shapeService, times(1)).getCopy();
        verify(inferenceService, times(0)).infer(any());
        verify(validationService, times(1)).validate(any(), any(), anyBoolean());
        verify(validationService, times(1)).modelPassedValidation(any());
    }

    @Test
    public void testPositiveValidationResultFromValidationService() throws Exception {
        String validationResultRdfJson = "{ \n" +
                "  \"_:dd2e80cb-ba8a-4f2e-b5be-c61ca4483546\" : { \n" +
                "    \"http://www.w3.org/1999/02/22-rdf-syntax-ns#type\" : [ { \n" +
                "      \"type\" : \"uri\" ,\n" +
                "      \"value\" : \"http://www.w3.org/ns/shacl#ValidationReport\"\n" +
                "    }\n" +
                "     ] ,\n" +
                "    \"http://www.w3.org/ns/shacl#conforms\" : [ { \n" +
                "      \"type\" : \"literal\" ,\n" +
                "      \"value\" : \"true\" ,\n" +
                "      \"datatype\" : \"http://www.w3.org/2001/XMLSchema#boolean\"\n" +
                "    }\n" +
                "     ]\n" +
                "  }\n" +
                "}\n";
        Model validationResult = ModelFactory.createDefaultModel().read(new StringReader(validationResultRdfJson), "", Lang.RDFJSON.getName());
        when(validationService.validate(any(), any(), anyBoolean())).thenReturn(validationResult);
        when(validationService.modelPassedValidation(any())).thenReturn(true);
        when(shapeService.getCopy()).thenReturn(ModelFactory.createDefaultModel());
        when(inferenceService.infer(any())).thenReturn(ModelFactory.createDefaultModel());

        ValidateContext validateContext = new ValidateContext();
        validateContext.setModel(EMPTY_MODEL_RDFJSON);

        mockMvc.perform(post("/validate").content(asJsonString(validateContext)).contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().is(HttpStatus.OK.value()))
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$.errorMessage").isEmpty())
                .andExpect(jsonPath("$.validJenaModelProvided").value(true))
                .andExpect(jsonPath("$.conformModelProvided").value(true))
                .andExpect(jsonPath("$.totalTimeForInferenceInMilliSeconds").value(greaterThan(-1)))
                .andExpect(jsonPath("$.totalTimeForValidationInMilliSeconds").value(greaterThan(-1)))
                .andExpect(jsonPath("$.requestId").isNotEmpty());

        verify(shapeService, times(1)).getCopy();
        verify(inferenceService, times(1)).infer(any());
        verify(validationService, times(1)).validate(any(), any(), anyBoolean());
        verify(validationService, times(1)).modelPassedValidation(any());
    }

    @Test
    public void testNegativeValidationResultFromValidationService() throws Exception {
        String validationResultRdfJson = "{\n" +
                "  \"_:0bbc9867-0ad7-48eb-b2b1-050bd06eec9a\" : {\n" +
                "    \"http://www.w3.org/ns/shacl#sourceConstraintComponent\" : [ {\n" +
                "      \"type\" : \"uri\" ,\n" +
                "      \"value\" : \"http://www.w3.org/ns/shacl#MinCountConstraintComponent\"\n" +
                "    }\n" +
                "    ] ,\n" +
                "    \"http://www.w3.org/ns/shacl#resultSeverity\" : [ {\n" +
                "      \"type\" : \"uri\" ,\n" +
                "      \"value\" : \"http://www.w3.org/ns/shacl#Violation\"\n" +
                "    }\n" +
                "    ] ,\n" +
                "    \"http://www.w3.org/1999/02/22-rdf-syntax-ns#type\" : [ {\n" +
                "      \"type\" : \"uri\" ,\n" +
                "      \"value\" : \"http://www.w3.org/ns/shacl#ValidationResult\"\n" +
                "    }\n" +
                "    ] ,\n" +
                "    \"http://www.w3.org/ns/shacl#resultMessage\" : [ {\n" +
                "      \"type\" : \"literal\" ,\n" +
                "      \"value\" : \"Less than 1 values\"\n" +
                "    }\n" +
                "    ] ,\n" +
                "    \"http://www.w3.org/ns/shacl#resultPath\" : [ {\n" +
                "      \"type\" : \"uri\" ,\n" +
                "      \"value\" : \"http://publications.europa.eu/ontology/cdm/cmr#lastModificationDate\"\n" +
                "    }\n" +
                "    ] ,\n" +
                "    \"http://www.w3.org/ns/shacl#sourceShape\" : [ {\n" +
                "      \"type\" : \"bnode\" ,\n" +
                "      \"value\" : \"_:4b0b82c0857df567476dd82202553654\"\n" +
                "    }\n" +
                "    ] ,\n" +
                "    \"http://www.w3.org/ns/shacl#focusNode\" : [ {\n" +
                "      \"type\" : \"uri\" ,\n" +
                "      \"value\" : \"http://publications.europa.eu/resource/authority/corporate-body/EURUN\"\n" +
                "    }\n" +
                "    ]\n" +
                "  }\n" +
                ",\n" +
                "  \"_:49cdd555-eebe-4162-832a-c37f587bcb8b\" : {\n" +
                "    \"http://www.w3.org/1999/02/22-rdf-syntax-ns#type\" : [ {\n" +
                "      \"type\" : \"uri\" ,\n" +
                "      \"value\" : \"http://www.w3.org/ns/shacl#ValidationReport\"\n" +
                "    }\n" +
                "    ] ,\n" +
                "    \"http://www.w3.org/ns/shacl#conforms\" : [ {\n" +
                "      \"type\" : \"literal\" ,\n" +
                "      \"value\" : \"false\" ,\n" +
                "      \"datatype\" : \"http://www.w3.org/2001/XMLSchema#boolean\"\n" +
                "    }\n" +
                "    ] ,\n" +
                "    \"http://www.w3.org/ns/shacl#result\" : [ {\n" +
                "      \"type\" : \"bnode\" ,\n" +
                "      \"value\" : \"_:0bbc9867-0ad7-48eb-b2b1-050bd06eec9a\"\n" +
                "    }\n" +
                "    , {\n" +
                "        \"type\" : \"bnode\" ,\n" +
                "        \"value\" : \"_:9b5993d2-1e0e-4d35-ad74-b410b9739742\"\n" +
                "      }\n" +
                "    ]\n" +
                "  }\n" +
                ",\n" +
                "  \"_:9b5993d2-1e0e-4d35-ad74-b410b9739742\" : {\n" +
                "    \"http://www.w3.org/ns/shacl#sourceConstraintComponent\" : [ {\n" +
                "      \"type\" : \"uri\" ,\n" +
                "      \"value\" : \"http://www.w3.org/ns/shacl#DatatypeConstraintComponent\"\n" +
                "    }\n" +
                "    ] ,\n" +
                "    \"http://www.w3.org/ns/shacl#resultSeverity\" : [ {\n" +
                "      \"type\" : \"uri\" ,\n" +
                "      \"value\" : \"http://www.w3.org/ns/shacl#Violation\"\n" +
                "    }\n" +
                "    ] ,\n" +
                "    \"http://www.w3.org/1999/02/22-rdf-syntax-ns#type\" : [ {\n" +
                "      \"type\" : \"uri\" ,\n" +
                "      \"value\" : \"http://www.w3.org/ns/shacl#ValidationResult\"\n" +
                "    }\n" +
                "    ] ,\n" +
                "    \"http://www.w3.org/ns/shacl#resultMessage\" : [ {\n" +
                "      \"type\" : \"literal\" ,\n" +
                "      \"value\" : \"Value does not have datatype xsd:positiveInteger\"\n" +
                "    }\n" +
                "    ] ,\n" +
                "    \"http://www.w3.org/ns/shacl#resultPath\" : [ {\n" +
                "      \"type\" : \"uri\" ,\n" +
                "      \"value\" : \"http://publications.europa.eu/ontology/cdm#official-journal_volume\"\n" +
                "    }\n" +
                "    ] ,\n" +
                "    \"http://www.w3.org/ns/shacl#value\" : [ {\n" +
                "      \"type\" : \"literal\" ,\n" +
                "      \"value\" : \"1\" ,\n" +
                "      \"datatype\" : \"http://www.w3.org/2001/XMLSchema#nonNegativeInteger\"\n" +
                "    }\n" +
                "    ] ,\n" +
                "    \"http://www.w3.org/ns/shacl#sourceShape\" : [ {\n" +
                "      \"type\" : \"bnode\" ,\n" +
                "      \"value\" : \"_:546c55829576d355cb80eb1ceed8d615\"\n" +
                "    }\n" +
                "    ] ,\n" +
                "    \"http://www.w3.org/ns/shacl#focusNode\" : [ {\n" +
                "      \"type\" : \"uri\" ,\n" +
                "      \"value\" : \"http://cellar-dev.publications.europa.eu/resource/cellar/ba72ae05-3be4-11e8-bab6-01aa75ed71a1\"\n" +
                "    }\n" +
                "    ]\n" +
                "  }\n" +
                "}\n";
        Model validationResult = ModelFactory.createDefaultModel().read(new StringReader(validationResultRdfJson), "", Lang.RDFJSON.getName());
        when(validationService.validate(any(), any(), anyBoolean())).thenReturn(validationResult);
        when(shapeService.getCopy()).thenReturn(ModelFactory.createDefaultModel());
        when(inferenceService.infer(any())).thenReturn(ModelFactory.createDefaultModel());

        ValidateContext validateContext = new ValidateContext();
        validateContext.setModel(EMPTY_MODEL_RDFJSON);

        mockMvc.perform(post("/validate").content(asJsonString(validateContext)).contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().is(HttpStatus.OK.value()))
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$.errorMessage").value(Matchers.startsWith("The model did not pass the SHACL validation. See response for the complete validation result")))
                .andExpect(jsonPath("$.validJenaModelProvided").value(true))
                .andExpect(jsonPath("$.conformModelProvided").value(false))
                .andExpect(jsonPath("$.totalTimeForInferenceInMilliSeconds").value(greaterThan(-1)))
                .andExpect(jsonPath("$.totalTimeForValidationInMilliSeconds").value(greaterThan(-1)))
                .andExpect(jsonPath("$.requestId").isNotEmpty());

        verify(shapeService, times(1)).getCopy();
        verify(inferenceService, times(1)).infer(any());
        verify(validationService, times(1)).validate(any(), any(), anyBoolean());
    }

    @Test
    public void whenApplicationExceptionInProductionEndpointThenInternalServerErrorExceptionIsReturned() throws Exception {
        when(validationService.validate(any(), any(), anyBoolean())).thenReturn(ModelFactory.createDefaultModel());
        when(shapeService.getCopy()).thenReturn(ModelFactory.createDefaultModel());
        when(inferenceService.infer(any())).thenThrow(
                new RuntimeException("exception thrown for testing"));

        ValidateContext validateContext = new ValidateContext();
        validateContext.setModel(EMPTY_MODEL_RDFJSON);

        mockMvc.perform(post("/validate").content(asJsonString(validateContext)).contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().is(HttpStatus.INTERNAL_SERVER_ERROR.value()))
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$.validJenaModelProvided").value(true))
                .andExpect(jsonPath("$.conformModelProvided").value(false))
                .andExpect(jsonPath("$.errorMessage").value(
                        "exception thrown for testing"))
                .andExpect(jsonPath("$.requestId").isNotEmpty());
        verify(shapeService, times(1)).getCopy();
        verify(inferenceService, times(1)).infer(any());
        verify(validationService, times(0)).validate(any(), any(), anyBoolean());
    }

    @Test
    public void whenApplicationExceptionInTestEndpointThenInternalServerErrorExceptionIsReturned() throws Exception {
        when(validationService.validate(any(), any(), anyBoolean())).thenReturn(ModelFactory.createDefaultModel());
        when(shapeService.getCopy()).thenReturn(ModelFactory.createDefaultModel());
        when(inferenceService.infer(any())).thenThrow(
                new RuntimeException("exception thrown for testing"));

        ValidateTestContext validateTestContext = new ValidateTestContext();
        validateTestContext.setModel(EMPTY_MODEL_RDFJSON);
        validateTestContext.setInfer(true);

        mockMvc.perform(post("/validate/test").content(asJsonString(validateTestContext)).contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().is(HttpStatus.INTERNAL_SERVER_ERROR.value()))
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$.validJenaModelProvided").value(true))
                .andExpect(jsonPath("$.conformModelProvided").value(false))
                .andExpect(jsonPath("$.errorMessage").value(
                        "exception thrown for testing"))
                .andExpect(jsonPath("$.requestId").isNotEmpty());
        verify(shapeService, times(1)).getCopy();
        verify(inferenceService, times(1)).infer(any());
        verify(validationService, times(0)).validate(any(), any(), anyBoolean());
    }

    @Test
    public void testPositiveValidationResultFromValidationTestFileService() throws Exception {
        when(validationService.validate(any(), any(), anyBoolean())).thenReturn(ModelFactory.createDefaultModel());
        when(inferenceService.infer(any())).thenThrow(new RuntimeException("exception thrown for testing"));
        when(modelFileExtensionService.isSupported(any(String.class))).thenReturn(true);
        when(modelFileExtensionService.getLang("ttl")).thenReturn(Lang.TURTLE);

        String modelString = "@prefix ex: <http://example.com/ns#> . ex:Alice a ex:Person";
        String shapesString = "@prefix ex: <http://example.com/ns#> . ex:Alice a ex:Person";
        MockMultipartFile modelFile = new MockMultipartFile("modelFile", "model.ttl", "text/turtle", modelString.getBytes());
        MockMultipartFile shapesFile = new MockMultipartFile("shapesFile", "shapes.ttl", "text/turtle", shapesString.getBytes());

        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/validate/test/file")
                .file(modelFile)
                .file(shapesFile)
                .param("infer", "true")
                .param("validateShapes", "false"))
                .andExpect(status().is(HttpStatus.INTERNAL_SERVER_ERROR.value()))
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$.validJenaModelProvided").value(true))
                .andExpect(jsonPath("$.conformModelProvided").value(false))
                .andExpect(jsonPath("$.errorMessage").value("exception thrown for testing"))
                .andExpect(jsonPath("$.requestId").isNotEmpty());
        verify(inferenceService, times(1)).infer(any());
        verify(validationService, times(0)).validate(any(), any(), anyBoolean());

    }
}