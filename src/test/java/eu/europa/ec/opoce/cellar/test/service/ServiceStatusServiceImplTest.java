/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.service
 *             FILE : ServiceStatusServiceImplTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 10 12, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-10-12 14:43:26 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.test.service;

import eu.europa.ec.opoce.cellar.service.InferenceService;
import eu.europa.ec.opoce.cellar.service.InferenceServiceImpl;
import eu.europa.ec.opoce.cellar.service.ServiceStatusRetrievable;
import eu.europa.ec.opoce.cellar.service.ServiceStatusServiceImpl;
import eu.europa.ec.opoce.cellar.service.ShapeService;
import eu.europa.ec.opoce.cellar.service.ShapeServiceImpl;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author ARHS Developments
 */
public class ServiceStatusServiceImplTest {

    private ServiceStatusServiceImpl serviceStatusService;

    @Test
    public void noServiceImplementsInterfaceOfServiceStatus() throws Exception {
        serviceStatusService = new ServiceStatusServiceImpl(new ArrayList<>());
        List<Object> serviceStatusAsObject = serviceStatusService.getServiceStatus();
        assertNotNull(serviceStatusAsObject);
        assertEquals(0, serviceStatusAsObject.size());
    }

    @Test
    public void objectsListContainServiceStatus() throws Exception {
        List<ServiceStatusRetrievable> serviceStatusRetrievableList = new ArrayList<>();

        ShapeService shapeService = mock(ShapeServiceImpl.class);
        when(shapeService.getStatus()).thenReturn(new Object());
        serviceStatusRetrievableList.add(shapeService);

        InferenceService inferenceService = mock(InferenceServiceImpl.class);
        when(inferenceService.getStatus()).thenReturn(new Object());
        serviceStatusRetrievableList.add(inferenceService);

        serviceStatusService = new ServiceStatusServiceImpl(serviceStatusRetrievableList);
        List<Object> serviceStatusAsObject = serviceStatusService.getServiceStatus();
        assertNotNull(serviceStatusAsObject);
        assertEquals(2, serviceStatusAsObject.size());
    }
}