/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.controller
 *             FILE : ServiceStatusControllerTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 10 12, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-10-12 14:55:57 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.test.controller;

import eu.europa.ec.opoce.cellar.controller.ServiceStatusController;
import eu.europa.ec.opoce.cellar.service.ServiceStatusServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author ARHS Developments
 */
@WebMvcTest(ServiceStatusController.class)
public class ServiceStatusControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ServiceStatusServiceImpl serviceStatusService;

    @Test
    public void noServiceStatusRetrievableThenEmptyJson() throws Exception {
        when(serviceStatusService.getServiceStatusRetrievableList()).thenReturn(new ArrayList<>());
        when(serviceStatusService.getServiceStatus()).thenCallRealMethod();

        mockMvc.perform(get("/status"))
                .andExpect(status().is(HttpStatus.OK.value()))
                .andExpect(content().contentType("application/json;charset=UTF-8"));
        System.out.println("test");
    }

    @Test
    public void oneServiceStatusRetrievableThenJsonContainsArrayOfSingleServiceStatus() throws Exception {
        Map<String, String> dummyMapForJson = new HashMap<>();
        dummyMapForJson.put("key", "value");

        when(serviceStatusService.getServiceStatusRetrievableList()).thenReturn(Collections.singletonList(() -> dummyMapForJson));
        when(serviceStatusService.getServiceStatus()).thenCallRealMethod();

        mockMvc.perform(get("/status"))
                .andExpect(status().is(HttpStatus.OK.value()))
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$[0].key").value("value"));
    }

    @Test
    public void multipleServiceStatusRetrievableThenJsonContainsArrayOfMultipleServiceStatus() throws Exception {
        Map<String, String> dummyMapForJson = new HashMap<>();
        dummyMapForJson.put("key", "value");

        Map<String, String> dummyMapForJsonTwo = new HashMap<>();
        dummyMapForJsonTwo.put("key2", "value2");

        when(serviceStatusService.getServiceStatusRetrievableList()).thenReturn(Arrays.asList(
                () -> dummyMapForJson,
                () -> dummyMapForJsonTwo
        ));
        when(serviceStatusService.getServiceStatus()).thenCallRealMethod();

        mockMvc.perform(get("/status"))
                .andExpect(status().is(HttpStatus.OK.value()))
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$[0].key").value("value"))
                .andExpect(jsonPath("$[1].key2").value("value2"));
    }
}