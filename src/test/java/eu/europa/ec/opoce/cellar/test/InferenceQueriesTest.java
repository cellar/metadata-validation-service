/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar
 *             FILE : InferenceQueriesTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 10 12, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-10-12 16:10:02 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.test;

import eu.europa.ec.opoce.cellar.configuration.InferenceQueries;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


/**
 * @author ARHS Developments
 */
public class InferenceQueriesTest {

    @Test
    public void emptyInferenceQueriesAreEqual() throws Exception {
        InferenceQueries inferenceQueriesOne = new InferenceQueries();
        InferenceQueries inferenceQueriesTwo = new InferenceQueries();
        assertEquals(inferenceQueriesOne, inferenceQueriesTwo);
        assertEquals(inferenceQueriesOne.hashCode(), inferenceQueriesTwo.hashCode());
    }

    @Test
    public void whenSettingNullQueriesThenEmptyListCreated() throws Exception {
        InferenceQueries inferenceQueries = new InferenceQueries();
        assertNotNull(inferenceQueries.getQueries());
        inferenceQueries.setQueries(null);
        assertNotNull(inferenceQueries.getQueries());
    }

    @Test
    public void inferenceQueriesFailClassTestEquals() throws Exception {
        assertNotEquals(new InferenceQueries(), "");
    }
    @Test
    public void inferenceQueriesEqualityDoesNotCareAboutListOrder() throws Exception {
        List<String> queriesOne = Arrays.asList("b", "a", "c");
        InferenceQueries inferenceQueriesOne = new InferenceQueries();
        inferenceQueriesOne.setQueries(queriesOne);

        List<String> queriesTwo = Arrays.asList("a", "c", "b");
        InferenceQueries inferenceQueriesTwo = new InferenceQueries();
        inferenceQueriesTwo.setQueries(queriesTwo);

        List<String> queriesThree = Arrays.asList("a", "b", "c");
        InferenceQueries inferenceQueriesThree = new InferenceQueries();
        inferenceQueriesThree.setQueries(queriesThree);

        assertNotEquals(queriesOne, queriesTwo);
        assertNotEquals(queriesOne.hashCode(), queriesTwo.hashCode());
        assertNotEquals(queriesOne, queriesThree);
        assertNotEquals(queriesOne.hashCode(), queriesThree.hashCode());

        assertEquals(inferenceQueriesOne, inferenceQueriesTwo);
        assertEquals(inferenceQueriesOne.hashCode(), inferenceQueriesTwo.hashCode());
        assertEquals(inferenceQueriesOne, inferenceQueriesThree);
        assertEquals(inferenceQueriesOne.hashCode(), inferenceQueriesThree.hashCode());

        assertNotEquals(queriesOne.hashCode(), inferenceQueriesOne.hashCode());
        assertNotEquals(queriesTwo.hashCode(), inferenceQueriesTwo.hashCode());
        assertNotEquals(queriesThree.hashCode(), inferenceQueriesThree.hashCode());
    }
}