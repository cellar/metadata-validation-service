/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.test
 *             FILE : InferenceIT.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 12 22, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-12-22 13:02:14 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.it;

import eu.europa.ec.opoce.cellar.IntegrationTest;
import eu.europa.ec.opoce.cellar.TestUtils;
import eu.europa.ec.opoce.cellar.service.InferenceService;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.Lang;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.springframework.test.util.AssertionErrors.assertTrue;

/**
 * @author ARHS Developments
 */
@IntegrationTest
@ActiveProfiles("it")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class InferenceITests {

    @Autowired
    InferenceService inferenceService;

    @Test
    public void validInputModelNormalizationTest() throws Exception {
        Model inputModel = TestUtils.getModel("classpath:/eu/europa/ec/opoce/cellar/test/it/inference/mets_metadata.ttl");
        Model normalizedModel = inferenceService.normalize(inputModel);
        Model normalizedModelComparison = TestUtils.getModel("classpath:/eu/europa/ec/opoce/cellar/test/it/inference/mets_metadata_normalized.ttl");

        assertTrue("model should be isomorphic", normalizedModel.isIsomorphicWith(normalizedModelComparison));
    }

    // this test depends on the extended model in application.yml : file.extendedModel
    // if this model change, the test will fail so it is needed to update the model in "inferredModelPath" to reflect
    // the new model. If the default extended model did not change and this test fails, it is likely that there is
    // a regression.
    @Test
    public void validInputModelInferenceTest() throws Exception {
        Model inputModel = TestUtils.getModel("classpath:/eu/europa/ec/opoce/cellar/test/it/inference/mets_metadata.ttl");
        Model inferredModel = inferenceService.infer(inputModel);
        Model inferredModelComparison = TestUtils.getModel("classpath:/eu/europa/ec/opoce/cellar/test/it/inference/mets_metadata_inferred.ttl");

        assertTrue("model should be isomorphic", inferredModel.isIsomorphicWith(inferredModelComparison));
    }
}
