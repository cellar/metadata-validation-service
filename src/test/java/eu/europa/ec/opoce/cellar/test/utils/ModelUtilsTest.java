package eu.europa.ec.opoce.cellar.test.utils;

import eu.europa.ec.opoce.cellar.TestUtils;
import eu.europa.ec.opoce.cellar.utils.ModelUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.Lang;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ModelUtilsTest {
    private static String MODEL_CONTENT_TURTLE;

    @BeforeAll
    public static void oneTimeSetUp() throws Exception {
        MODEL_CONTENT_TURTLE = TestUtils.getModelAsString("classpath:/eu/europa/ec/opoce/cellar/test/utils/model.ttl");
    }

    @Test
    public void nullArguments_transform_emptyStringReturned() throws Exception {
        assertEquals("", ModelUtils.writeModel(null, null));
    }

    @Test
    public void nullModelAndValidArguments_transform_emptyStringReturned() throws Exception {
        assertEquals("", ModelUtils.writeModel(null, Lang.RDFJSON));
    }

    @Test
    public void emptyModelAndNullType_transform_defaultRdfJsonReturned() throws Exception {
        Model model = ModelFactory.createDefaultModel();
        String rdfJsonOutputModel = ModelUtils.writeModel(model, null);
        assertEquals("{ }", rdfJsonOutputModel, "if type not provided, should default to RDF/JSON output format");
    }

    @Test
    public void emptyModelAndRdfJsonType_transform_rdfJsonReturned() throws Exception {
        Model model = ModelFactory.createDefaultModel();
        String rdfJsonOutputModel = ModelUtils.writeModel(model, Lang.RDFJSON);
        assertEquals("{ }", rdfJsonOutputModel);
    }

    @Test
    public void emptyModelAndTurtleType_transform_turtleOutputReturned() throws Exception {
        Model model = ModelFactory.createDefaultModel();
        String rdfJsonOutputModel = ModelUtils.writeModel(model, Lang.TURTLE);
        assertEquals("", rdfJsonOutputModel);
    }

    @Test
    public void nonEmptyModelAndRdfJsonType_transform_rdfJsonReturned() throws Exception {
        Model model = ModelFactory.createDefaultModel();
        model.read(new StringReader(MODEL_CONTENT_TURTLE), "", Lang.TURTLE.getLabel());
        String rdfJsonOutputModel = ModelUtils.writeModel(model, Lang.RDFJSON);
        assertEquals("{ \n" +
                "  \"http://example.com/ns#Alice\" : { \n" +
                "    \"http://example.com/ns#LastName\" : [ { \n" +
                "      \"type\" : \"literal\" ,\n" +
                "      \"value\" : \"Test\"\n" +
                "    }\n" +
                "     ] ,\n" +
                "    \"http://www.w3.org/1999/02/22-rdf-syntax-ns#type\" : [ { \n" +
                "      \"type\" : \"uri\" ,\n" +
                "      \"value\" : \"http://www.w3.org/2002/07/owl#Thing\"\n" +
                "    }\n" +
                "    , { \n" +
                "      \"type\" : \"uri\" ,\n" +
                "      \"value\" : \"http://example.com/ns#Person\"\n" +
                "    }\n" +
                "     ] ,\n" +
                "    \"http://example.com/ns#FirstName\" : [ { \n" +
                "      \"type\" : \"literal\" ,\n" +
                "      \"value\" : \"Alice\"\n" +
                "    }\n" +
                "     ]\n" +
                "  }\n" +
                "}\n", rdfJsonOutputModel);
    }

    @Test
    public void nonEmptyModelAndTurtleType_transform_rdfJsonReturned() throws Exception {
        Model model = ModelFactory.createDefaultModel();
        model.read(new StringReader(MODEL_CONTENT_TURTLE), "", Lang.TURTLE.getLabel());
        String turtleOutputModel = ModelUtils.writeModel(model, Lang.TURTLE);
        assertEquals("@prefix ex:  <http://example.com/ns#> .\n" +
                "@prefix owl: <http://www.w3.org/2002/07/owl#> .\n" +
                "\n" +
                "ex:Alice  a           owl:Thing , ex:Person ;\n" +
                "        ex:FirstName  \"Alice\" ;\n" +
                "        ex:LastName   \"Test\" .\n", turtleOutputModel);
    }

    @Test
    public void nullArguments_parseModel_optionalEmptyReturned() throws Exception {
        Optional<Model> model = ModelUtils.parseModel(null, null);
        assertEquals(Optional.empty(), model);
    }

    @Test
    public void nullModelStringAndValidType_parseModel_optionalEmptyReturned() throws Exception {
        Optional<Model> model = ModelUtils.parseModel(null, Lang.RDFJSON);
        assertEquals(Optional.empty(), model);
    }

    @Test
    public void emptyModelStringAndValidType_parseModel_emptyOptionalReturned() throws Exception {
        Optional<Model> model = ModelUtils.parseModel("", Lang.RDFJSON);
        assertEquals(Optional.empty(), model);
    }

    @Test
    public void emptyModelStringAndEmptyType_parseModel_emptyOptionalReturned() throws Exception {
        Optional<Model> model = ModelUtils.parseModel("", null);
        assertEquals(Optional.empty(), model);
    }

    @Test
    public void nonEmptyModelStringAndValidType_parseModel_modelReturned() throws Exception {
        Optional<Model> model = ModelUtils.parseModel(MODEL_CONTENT_TURTLE, Lang.TURTLE);
        assertTrue(model.isPresent());
    }

    @Test
    public void blankFilename_getInputFormatFromName_null() throws Exception {
        assertNull(ModelUtils.getInputFormatFromName(null));
        assertNull(ModelUtils.getInputFormatFromName(""));
    }

    @Test
    public void filenameExtensionNotInValidLang_getInputFormatFromName_null() throws Exception {
        assertNull(ModelUtils.getInputFormatFromName("test.txt"));
        assertNull(ModelUtils.getInputFormatFromName("no_extension"));
    }

    @Test
    public void filenameExtensionNotRecognizedAndFileExists_parseModel_optionalEmpty() throws Exception {
        Optional<Model> optionalModel = ModelUtils.parseModel(
                TestUtils.extract("classpath:/eu/europa/ec/opoce/cellar/test/utils/invalid_extension.txt"));
        assertFalse(optionalModel.isPresent());
    }

    @Test
    public void pathDoesNotExist_parseModel_optionalEmpty() throws Exception {
        Optional<Model> optionalModel = ModelUtils.parseModel(Paths.get("notfound.rj"));
        assertFalse(optionalModel.isPresent());
    }

    @Test
    public void fileNameValidAndValidFile_parseModel_modelReturned() throws Exception {
        Optional<Model> optionalModel = ModelUtils.parseModel(
                TestUtils.extract("classpath:/eu/europa/ec/opoce/cellar/test/utils/model.rj"));
        assertTrue(optionalModel.isPresent());
    }

    @Test
    public void fileNameValidAndFileProblem_parseModel_optionalEmpty() throws Exception {
        Path path = Paths.get(System.getProperty("java.io.tmpdir"), "test_read_file_perm.txt");
        Files.deleteIfExists(path);
        Files.createFile(path);
        assertTrue(path.toFile().setReadable(false));

        Optional<Model> optionalModel = ModelUtils.parseModel(path);
        assertFalse(optionalModel.isPresent());
        Files.deleteIfExists(path);
    }

    @Test
    public void testConvertNullModel() throws Exception {
        final String model = ModelUtils.convertModel(null, Lang.TTL, Lang.RDFJSON);
        assertEquals("", model);
    }

    @Test
    public void testConvertEmptyModel() throws Exception {
        final String model = ModelUtils.convertModel("", Lang.TTL, Lang.RDFJSON);
        assertEquals("", model);
    }

    @Test
    public void testConvertModelWithoutSourceLang() throws Exception {
        final String model = ModelUtils.convertModel(
                TestUtils.getModelAsString("classpath:/eu/europa/ec/opoce/cellar/test/utils/model.ttl"), null, Lang.TTL);
        assertEquals("", model);
    }

    @Test
    public void testConvertModelDefaultTargetLang() throws Exception {
        final String model = ModelUtils.convertModel(
                TestUtils.getModelAsString("classpath:/eu/europa/ec/opoce/cellar/test/utils/model.ttl"), Lang.TTL, null);
        assertEquals("{ \n" +
                "  \"http://example.com/ns#Alice\" : { \n" +
                "    \"http://example.com/ns#LastName\" : [ { \n" +
                "      \"type\" : \"literal\" ,\n" +
                "      \"value\" : \"Test\"\n" +
                "    }\n" +
                "     ] ,\n" +
                "    \"http://www.w3.org/1999/02/22-rdf-syntax-ns#type\" : [ { \n" +
                "      \"type\" : \"uri\" ,\n" +
                "      \"value\" : \"http://www.w3.org/2002/07/owl#Thing\"\n" +
                "    }\n" +
                "    , { \n" +
                "      \"type\" : \"uri\" ,\n" +
                "      \"value\" : \"http://example.com/ns#Person\"\n" +
                "    }\n" +
                "     ] ,\n" +
                "    \"http://example.com/ns#FirstName\" : [ { \n" +
                "      \"type\" : \"literal\" ,\n" +
                "      \"value\" : \"Alice\"\n" +
                "    }\n" +
                "     ]\n" +
                "  }\n" +
                "}\n", model);
    }

    @Test
    public void filenameExtensionInValidLang_getInputFormatFromName_langReturned() throws Exception {
        assertEquals(Lang.RDFXML, ModelUtils.getInputFormatFromName("file.xml"));
        assertEquals(Lang.NTRIPLES, ModelUtils.getInputFormatFromName("file.nt"));
        assertEquals(Lang.NT, ModelUtils.getInputFormatFromName("file.nt"));
        assertEquals(Lang.N3, ModelUtils.getInputFormatFromName("file.n3"));
        assertEquals(Lang.TURTLE, ModelUtils.getInputFormatFromName("file.ttl"));
        assertEquals(Lang.TTL, ModelUtils.getInputFormatFromName("file.ttl"));
        assertEquals(Lang.JSONLD, ModelUtils.getInputFormatFromName("file.jsonld"));
        assertEquals(Lang.RDFJSON, ModelUtils.getInputFormatFromName("file.rj"));
        assertEquals(Lang.NQUADS, ModelUtils.getInputFormatFromName("file.nq"));
        assertEquals(Lang.NQ, ModelUtils.getInputFormatFromName("file.nq"));
        assertEquals(Lang.TRIG, ModelUtils.getInputFormatFromName("file.trig"));
        assertEquals(Lang.RDFTHRIFT, ModelUtils.getInputFormatFromName("file.rt"));
        assertEquals(Lang.CSV, ModelUtils.getInputFormatFromName("file.csv"));
        assertEquals(Lang.TRIX, ModelUtils.getInputFormatFromName("file.trix"));
    }

    @Test
    public void testQuietlyClosingModel() {
        Model model = ModelFactory.createDefaultModel();
        assertFalse(model.isClosed());
        ModelUtils.closeQuietly(model);
        assertTrue(model.isClosed());
    }

    @Test
    public void getFocusNodeTest() throws Exception {
        Model model = TestUtils.getModel("classpath:/eu/europa/ec/opoce/cellar/test/utils/focus-node-model.rdf");

        Set<String> focusNodes = ModelUtils.getFocusNodes(model);

        assertEquals(3, focusNodes.size());
        assertTrue(focusNodes.contains("http://datashapes.org/sh/tests/core/property/datatype-001.test#InvalidResource1"));
        assertTrue(focusNodes.contains("http://datashapes.org/sh/tests/core/property/datatype-001.test#InvalidResource2"));
        assertTrue(focusNodes.contains("http://datashapes.org/sh/tests/core/property/datatype-001.test#InvalidResource3"));
    }
}