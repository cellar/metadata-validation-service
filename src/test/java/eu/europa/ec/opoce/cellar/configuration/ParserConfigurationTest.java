package eu.europa.ec.opoce.cellar.configuration;


import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ParserConfigurationTest {

    @Test
    public void getExceptionPatternsTest() {
        final ParserConfiguration parserConfiguration = new ParserConfiguration();
        parserConfiguration.setExceptionPatterns(Collections.singletonList("\\w+"));

        assertEquals(1, parserConfiguration.getExceptionPatterns().size());
        assertEquals(1, parserConfiguration.getCompiledExceptionPatterns().size());
        assertEquals("\\w+", parserConfiguration.getExceptionPatterns().get(0));
        assertEquals("\\w+", parserConfiguration.getCompiledExceptionPatterns().get(0).toString());
    }
}