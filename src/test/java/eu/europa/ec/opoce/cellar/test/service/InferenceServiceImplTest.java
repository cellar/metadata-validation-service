package eu.europa.ec.opoce.cellar.test.service;

import eu.europa.ec.opoce.cellar.configuration.FileConfiguration;
import eu.europa.ec.opoce.cellar.configuration.InferenceQueries;
import eu.europa.ec.opoce.cellar.service.ConfigurationLoader;
import eu.europa.ec.opoce.cellar.service.InferenceService;
import eu.europa.ec.opoce.cellar.service.InferenceServiceImpl;
import eu.europa.ec.opoce.cellar.service.S3ModelService;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static eu.europa.ec.opoce.cellar.TestUtils.getModel;
import static eu.europa.ec.opoce.cellar.TestUtils.getModelAsString;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class InferenceServiceImplTest {

    private InferenceService inferenceService;
    private static InferenceQueries inferenceQueries;
    private FileConfiguration fileConfiguration;

    @Mock
    private S3ModelService s3ModelService;

    private ConfigurationLoader configurationLoader;

    @BeforeAll
    public static void oneTimeSetUp() throws Exception {
        inferenceQueries = new InferenceQueries();

        List<String> sparqlQueries = new ArrayList<>();
        sparqlQueries.add("" +
                "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
                "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n" +
                "INSERT {\n" +
                "    ?subclass_instance rdf:type ?sup_class\n" +
                "}\n" +
                "WHERE {\n" +
                "    ?sub_class rdfs:subClassOf ?sup_class.\n" +
                "    ?subclass_instance rdf:type ?sub_class\n" +
                "    FILTER ((?sup_class!=rdfs:Resource) && (?sup_class!=rdfs:Class) && (?sup_class!=rdf:Property) && ((?subclass_instance!=rdf:nil) || (?sup_class!=rdf:List)))\n" +
                "}");
        sparqlQueries.add("" +
                "PREFIX owl: <http://www.w3.org/2002/07/owl#>\n" +
                "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
                "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n" +
                "INSERT {\n" +
                "    ?sub_prop_domain ?sup_prop ?sub_prop_range\n" +
                "}\n" +
                "WHERE {\n" +
                "    ?sub_prop rdfs:subPropertyOf ?sup_prop.\n" +
                "    ?sub_prop_domain ?sub_prop ?sub_prop_range\n" +
                "}");
        sparqlQueries.add("" +
                "PREFIX cmr: <http://publications.europa.eu/ontology/cdm/cmr#>\n" +
                "PREFIX owl: <http://www.w3.org/2002/07/owl#>\n" +
                "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
                "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n" +
                "INSERT {\n" +
                "    ?inverse_prop_range ?inverse_prop2 ?inverse_prop_domain\n" +
                "}\n" +
                "WHERE {\n" +
                "    ?inverse_prop1 owl:inverseOf ?inverse_prop2.\n" +
                "    ?inverse_prop_domain ?inverse_prop1 ?inverse_prop_range\n" +
                "}");
        inferenceQueries.setQueries(sparqlQueries);
    }

    @BeforeEach
    public void setUp() {
        configurationLoader = new ConfigurationLoader(s3ModelService);
        fileConfiguration = new FileConfiguration();
        fileConfiguration.setExtendedModel("classpath:/eu/europa/ec/opoce/cellar/test/inference/impl/extendedModel/validExtendedModel.ttl");
        fileConfiguration.setInferenceModel("classpath:/eu/europa/ec/opoce/cellar/test/inference/impl/ontology/inferred_ontology_model.rdf");
        inferenceService = new InferenceServiceImpl(fileConfiguration, inferenceQueries, configurationLoader);
    }

    @Test
    public void noInferenceQueriesProvidedThenEmptyListQueriesUsed() {
        InferenceQueries queries = new InferenceQueries();
        inferenceService = new InferenceServiceImpl(fileConfiguration, queries, configurationLoader);
        assertTrue(inferenceService.getStatus() instanceof InferenceServiceImpl.InferenceServiceStatus);
        InferenceServiceImpl.InferenceServiceStatus status = (InferenceServiceImpl.InferenceServiceStatus) inferenceService.getStatus();
        assertNotNull(status.getInferenceQueries());
        assertEquals(0, status.getInferenceQueries().getQueries().size());
    }

    @Test
    public void queriesFoundInYamlAndSuccessfullyLoaded() {
        assertTrue(inferenceService.getStatus() instanceof InferenceServiceImpl.InferenceServiceStatus);
        InferenceServiceImpl.InferenceServiceStatus status = (InferenceServiceImpl.InferenceServiceStatus) inferenceService.getStatus();
        assertNotNull(status.getInferenceQueries());
        assertEquals(3, status.getInferenceQueries().getQueries().size());
    }

    @Test
    public void inputModelCannotBeNull() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> inferenceService.infer(null),
                "Input model cannot be null for the inference");
    }

    @Test
    public void emptyModel_infer_emptyModelReturned() {
        Model inferredModel = inferenceService.infer(ModelFactory.createDefaultModel());
        assertNotNull(inferredModel);
    }

    @Test
    public void inputModelCannotBeNullForNormalization() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> inferenceService.normalize(null),
                "Input model for normalization cannot be null");
    }

    @Test
    public void emptyModel_normalize_emptyModelReturned() {
        Model model = inferenceService.normalize(ModelFactory.createDefaultModel());
        assertNotNull(model);
        assertTrue(model.isEmpty());
    }

    @Test
    public void nonEmptyModelWithoutSameAs_normalize_inputModelReturned() {
        Model m = getModel("classpath:/eu/europa/ec/opoce/cellar/test/inference/impl/models/model.owl");
        Model normalizedModel = inferenceService.normalize(m);
        assertTrue(m.isIsomorphicWith(normalizedModel));
    }

    @Test
    public void nonEmptyModelWithSameAsAndMatchPattern_normalize_normalizedModelreturned() {
        Model m = getModel("classpath:/eu/europa/ec/opoce/cellar/test/inference/impl/models/model-with-cellar-sameAs.rdf");
        Query askQuery = QueryFactory.create("" +
                "ASK " +
                "WHERE {" +
                "<http://publications.europa.eu/resource/oj/JOL_2006_088_R_0063_01> " +
                "<http://publications.europa.eu/ontology/cdm#work_resource> " +
                "<http://publications.europa.eu/resource/oj/JOL_2006_088_R_0063_01>" +
                "}");
        QueryExecution queryExecution = QueryExecutionFactory.create(askQuery, m);
        assertTrue(queryExecution.execAsk(),
                "before the normalization, <http://publications.europa.eu/resource/oj/JOL_2006_088_R_0063_01> should be found in model");

        Model normalizedModel = inferenceService.normalize(m);
        queryExecution = QueryExecutionFactory.create(askQuery, normalizedModel);
        assertFalse(queryExecution.execAsk(),
                "after the normalization, <http://publications.europa.eu/resource/oj/JOL_2006_088_R_0063_01> should not be found anymore");

        askQuery = QueryFactory.create("" +
                "ASK " +
                "WHERE {" +
                "<http://publications.europa.eu/resource/cellar/68d5b365-3a11-11e7-828a-01aa75ed71a1> " +
                "<http://publications.europa.eu/ontology/cdm#work_resource> " +
                "<http://publications.europa.eu/resource/cellar/68d5b365-3a11-11e7-828a-01aa75ed71a1>" +
                "}");
        queryExecution = QueryExecutionFactory.create(askQuery, normalizedModel);
        assertTrue(queryExecution.execAsk(),
                "http://publications.europa.eu/resource/cellar/68d5b365-3a11-11e7-828a-01aa75ed71a1 should be present in normalized model");
        assertFalse(m.isIsomorphicWith(normalizedModel), "model should not be isomorphicwith normalized model if same as with cellar are present");
    }

    @Test
    public void nonEmptyModelWithSameAsAndMatchPattern_infer_returnInferredAndNormalizedModel() {
        Model m = getModel("classpath:/eu/europa/ec/opoce/cellar/test/inference/impl/models/model-with-cellar-sameAs.rdf");

        Query askQuery = QueryFactory.create("" +
                "ASK " +
                "WHERE {" +
                "<http://publications.europa.eu/resource/cellar/68d5b365-3a11-11e7-828a-01aa75ed71a1> " +
                "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> " +
                "<http://publications.europa.eu/ontology/cdm#resource_legal>" +
                "}");
        QueryExecution queryExecution = QueryExecutionFactory.create(askQuery, m);
        assertFalse(queryExecution.execAsk(),
                "before the inference, <http://publications.europa.eu/resource/cellar/68d5b365-3a11-11e7-828a-01aa75ed71a1>, " +
                        "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://publications.europa.eu/ontology/cdm#resource_legal>" +
                        "should NOT be found in the model");

        Model inferredModel = inferenceService.infer(m);
        queryExecution = QueryExecutionFactory.create(askQuery, inferredModel);
        assertTrue(queryExecution.execAsk(),
                "after the inference, <http://publications.europa.eu/resource/cellar/68d5b365-3a11-11e7-828a-01aa75ed71a1>, " +
                        "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://publications.europa.eu/ontology/cdm#resource_legal>" +
                        "should be found in the model");
        assertFalse(m.isIsomorphicWith(inferredModel), "model should not be isomorphicwith normalized model if same as with cellar are present");
    }

    @Test
    public void optionalExtendedModelPathNotDefinedThenDefaultModelCreated() {
        FileConfiguration fileConfiguration = new FileConfiguration();
        fileConfiguration.setInferenceModel("classpath:/eu/europa/ec/opoce/cellar/test/inference/impl/ontology/inferred_ontology_model.rdf");
        fileConfiguration.setExtendedModel("");

        new InferenceServiceImpl(fileConfiguration, new InferenceQueries(), configurationLoader);

    }

    @Test
    public void optionalExtendedModelPathDefinedButCannotBeParsedThenApplicationShutdown() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            FileConfiguration fileConfiguration = new FileConfiguration();
            fileConfiguration.setInferenceModel("classpath:/eu/europa/ec/opoce/cellar/test/inference/impl/ontology/inferred_ontology_model.rdf");
            fileConfiguration.setExtendedModel("classpath:/eu/europa/ec/opoce/cellar/test/inference/impl/extendedModel/invalidExtendedModel.rdf");

            new InferenceServiceImpl(fileConfiguration, new InferenceQueries(), configurationLoader);
        });

    }

    @Test
    public void inferredOntologyModelCannotBeParsedThenApplicationShutDown() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            FileConfiguration fileConfiguration = new FileConfiguration();
            fileConfiguration.setInferenceModel("");
            fileConfiguration.setExtendedModel("classpath:/eu/europa/ec/opoce/cellar/test/inference/impl/extendedModel/validExtendedModel.ttl");

            new InferenceServiceImpl(fileConfiguration, new InferenceQueries(), configurationLoader);
        });
    }

    @Test
    public void inferenceServiceStatusContainsModelsPathAndInferenceQueries() {
        Object o = inferenceService.getStatus();
        assertTrue(o instanceof InferenceServiceImpl.InferenceServiceStatus);

        InferenceServiceImpl.InferenceServiceStatus inferenceServiceStatus = (InferenceServiceImpl.InferenceServiceStatus) o;
        assertEquals(inferenceServiceStatus.getExtendedModel(), fileConfiguration.getExtendedModel());
        assertEquals(inferenceServiceStatus.getInferredOntology(), fileConfiguration.getInferenceModel());
        assertEquals(inferenceServiceStatus.getInferenceQueries(), inferenceQueries);
    }

    @Test
    public void inferUsingOnlineModel() {
        final String inferenceModel = "https://s3-eu-west-1.amazonaws.com/op-metadata-validator/ontology/inferred_ontology_model.rdf";
        final String extendedModel = "https://s3-eu-west-1.amazonaws.com/op-metadata-validator/extendedModel/invalidExtendedModel.rdf";

        FileConfiguration fileConfiguration = new FileConfiguration();
        fileConfiguration.setInferenceModel(inferenceModel);
        fileConfiguration.setExtendedModel(extendedModel);

        when(s3ModelService.getModel(inferenceModel)).thenReturn(Optional.of(getModelAsString("classpath:/eu/europa/ec/opoce/cellar/test/inference/impl/ontology/inferred_ontology_model.rdf")));
        when(s3ModelService.getModel(extendedModel)).thenReturn(Optional.of(getModelAsString("classpath:/eu/europa/ec/opoce/cellar/test/inference/impl/ontology/inferred_ontology_model.rdf")));

        InferenceService inferenceService = new InferenceServiceImpl(fileConfiguration, new InferenceQueries(), configurationLoader);

        Model inferredModel = inferenceService.infer(ModelFactory.createDefaultModel());

        assertNotNull(inferredModel);
        assertTrue(inferredModel.toString().contains("ATTO_FD_600"));
    }

    @Test
    public void inferUsingEmptyInferenceModel() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            final String inferenceModel = "https://s3-eu-west-1.amazonaws.com/op-metadata-validator/ontology/inferred_ontology_model.rdf";
            final String extendedModel = "https://s3-eu-west-1.amazonaws.com/op-metadata-validator/extendedModel/extendedModel.rdf";

            FileConfiguration fileConfiguration = new FileConfiguration();
            fileConfiguration.setInferenceModel(inferenceModel);
            fileConfiguration.setExtendedModel(extendedModel);

            when(s3ModelService.getModel(inferenceModel)).thenReturn(Optional.empty());
            when(s3ModelService.getModel(extendedModel)).thenReturn(Optional.of(getModelAsString("classpath:/eu/europa/ec/opoce/cellar/test/inference/impl/ontology/inferred_ontology_model.rdf")));

            InferenceService inferenceService = new InferenceServiceImpl(fileConfiguration, new InferenceQueries(), configurationLoader);
            inferenceService.infer(ModelFactory.createDefaultModel());
        }, "The inference model located at 'https://s3-eu-west-1.amazonaws.com/op-metadata-validator/ontology/inferred_ontology_model.rdf' could not be parsed.");
    }

    @Test
    public void inferUsingEmptyExtendedModel() {
        final String inferenceModel = "https://s3-eu-west-1.amazonaws.com/op-metadata-validator/ontology/inferred_ontology_model.rdf";
        final String extendedModel = "https://s3-eu-west-1.amazonaws.com/op-metadata-validator/extendedModel/extendedModel.rdf";

        FileConfiguration fileConfiguration = new FileConfiguration();
        fileConfiguration.setInferenceModel(inferenceModel);
        fileConfiguration.setExtendedModel(extendedModel);

        when(s3ModelService.getModel(inferenceModel)).thenReturn(Optional.of(getModelAsString("classpath:/eu/europa/ec/opoce/cellar/test/inference/impl/ontology/inferred_ontology_model.rdf")));
        when(s3ModelService.getModel(extendedModel)).thenReturn(Optional.empty());

        InferenceService inferenceService = new InferenceServiceImpl(fileConfiguration, new InferenceQueries(), configurationLoader);
        Model inferredModel = inferenceService.infer(ModelFactory.createDefaultModel());

        assertNotNull(inferredModel);
    }

}