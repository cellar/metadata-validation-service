package eu.europa.ec.opoce.cellar;

import com.google.common.io.ByteStreams;
import com.google.common.io.CharStreams;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.RDFLanguages;
import org.springframework.core.io.ClassPathResource;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * @author ARHS Developments
 */
public final class TestUtils {

    private TestUtils() {
    }

    public static String getModelAsString(String resource) {
        try (InputStream is = new ClassPathResource(resource.replace("classpath:", "")).getInputStream()) {
            return CharStreams.toString(new InputStreamReader(is, StandardCharsets.UTF_8));
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public static Model getModel(String resource) {
        try (InputStream is = new ClassPathResource(resource.replace("classpath:", "")).getInputStream()) {
            Model m = ModelFactory.createDefaultModel();
            m.read(new InputStreamReader(is, StandardCharsets.UTF_8), "", RDFLanguages.filenameToLang(resource).getLabel());
            return m;
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public static Path extract(String classpathResource) {
        try {
            int idx = classpathResource.lastIndexOf('.');
            String name = classpathResource.substring(0, idx);
            String extension = classpathResource.substring(idx);
            Path f = Files.createTempFile(name.replaceAll("/", "_"), extension);
            ByteStreams.copy(new ClassPathResource(classpathResource.replace("classpath:", "")).getInputStream(), new FileOutputStream(f.toFile()));
            return f.toAbsolutePath();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
