package eu.europa.ec.opoce.cellar.test.service;

import com.google.common.io.CharStreams;
import eu.europa.ec.opoce.cellar.configuration.FileConfiguration;
import eu.europa.ec.opoce.cellar.service.ConfigurationLoader;
import eu.europa.ec.opoce.cellar.service.S3ModelService;
import eu.europa.ec.opoce.cellar.service.ShapeService;
import eu.europa.ec.opoce.cellar.service.ShapeServiceImpl;
import org.apache.jena.arq.querybuilder.AskBuilder;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.Optional;

import static eu.europa.ec.opoce.cellar.TestUtils.getModelAsString;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ShapeServiceImplTest {

    private static final String VALID_RDFJSON_SHAPE_PATH = "classpath:/eu/europa/ec/opoce/cellar/test/shape/impl/valid-rdfjson-shape.rj";
    private static final String VALID_TURTLE_SHAPE_PATH = "classpath:/eu/europa/ec/opoce/cellar/test/shape/impl/valid-turtle-shape.ttl";
    private static final String EMPTY_FILE_PATH = "classpath:/eu/europa/ec/opoce/cellar/test/shape/impl/empty-file.ttl";
    private static final String INVALID_EXTENSION_PATH = "classpath:/eu/europa/ec/opoce/cellar/test/shape/impl/invalid-extension-file.txt";
    private static final String NOT_MATCHING_EXTENSION_PATH = "classpath:/eu/europa/ec/opoce/cellar/test/shape/impl/invalid-extension-file.trix";
    private static final String S3_SHAPE_PATH = "https://s3-eu-west-1.amazonaws.com/op-metadata-validator/valid-turtle-shape.ttl";

    private ShapeServiceImpl shapeService;
    private FileConfiguration fileConfiguration;

    @Mock
    private S3ModelService s3ModelService;

    private ConfigurationLoader configurationLoader;

    @BeforeEach
    public void setUp() {
        fileConfiguration = mock(FileConfiguration.class);
        configurationLoader = new ConfigurationLoader(s3ModelService);
    }

    @Test
    public void nullShapesPathShapesNotLoadedThenApplicationShutdown() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            when(fileConfiguration.getShapes()).thenReturn(null);
            shapeService = new ShapeServiceImpl(fileConfiguration, configurationLoader);
        }, "Cannot load shapes with null file reference.");
    }

    @Test
    public void emptyShapesPathShapesNotLoadedThenApplicationShutdown() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            when(fileConfiguration.getShapes()).thenReturn("");
            shapeService = new ShapeServiceImpl(fileConfiguration, configurationLoader);
        });
    }

    @Test
    public void pathNotExistShapesNotLoadedThenApplicationShutdown() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            when(fileConfiguration.getShapes()).thenReturn("not exist");
            shapeService = new ShapeServiceImpl(fileConfiguration, configurationLoader);
        });
    }

    @Test
    public void validPathEmptyFileIsLoaded() {
        when(fileConfiguration.getShapes()).thenReturn(EMPTY_FILE_PATH);
        shapeService = new ShapeServiceImpl(fileConfiguration, configurationLoader);
        assertNotNull(shapeService.getCopy());
    }

    @Test
    public void validPathNonEmptyFileInvalidExtensionMatchThenApplicationShutdown() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            when(fileConfiguration.getShapes()).thenReturn(INVALID_EXTENSION_PATH);
            shapeService = new ShapeServiceImpl(fileConfiguration, configurationLoader);
        }, "Cannot find input format of file : " + Paths.get(INVALID_EXTENSION_PATH).toAbsolutePath());
    }

    @Test
    public void validPathNonEmptyFileNotMatchingExtensionMatchThenApplicationShutdown() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            when(fileConfiguration.getShapes()).thenReturn(NOT_MATCHING_EXTENSION_PATH);
            shapeService = new ShapeServiceImpl(fileConfiguration, configurationLoader);
        });
    }

    @Test
    public void validPathNonEmptyFileExtensionMatchButShapesAlreadyLoadedThenNoReload() {
        when(fileConfiguration.getShapes()).thenReturn(VALID_RDFJSON_SHAPE_PATH);
        shapeService = new ShapeServiceImpl(fileConfiguration, configurationLoader);
        assertNotNull(shapeService.getCopy());

        when(fileConfiguration.getShapes()).thenReturn(VALID_TURTLE_SHAPE_PATH);
        shapeService = new ShapeServiceImpl(fileConfiguration, configurationLoader);
        assertNotNull(shapeService.getCopy());
    }

    @Test
    public void shapeServiceStatusContainsShapesModelPath() {
        when(fileConfiguration.getShapes()).thenReturn(VALID_RDFJSON_SHAPE_PATH);
        shapeService = new ShapeServiceImpl(fileConfiguration, configurationLoader);
        assertNotNull(shapeService.getCopy());

        Object o = shapeService.getStatus();
        assertTrue(o instanceof ShapeServiceImpl.ShapeServiceStatus);

        ShapeServiceImpl.ShapeServiceStatus shapeServiceStatus = (ShapeServiceImpl.ShapeServiceStatus) o;
        assertEquals(shapeServiceStatus.getShapesModelPath(), fileConfiguration.getShapes());
    }

    @Test
    public void retrievedShapesAreAlwaysACopy() {
        when(fileConfiguration.getShapes()).thenReturn(VALID_RDFJSON_SHAPE_PATH);
        shapeService = new ShapeServiceImpl(fileConfiguration, configurationLoader);

        Model firstCopy = shapeService.getCopy();
        assertNotNull(firstCopy);

        Model secondCopy = shapeService.getCopy();
        assertNotNull(secondCopy);

        assertFalse(firstCopy == secondCopy);
        assertTrue(firstCopy.isIsomorphicWith(secondCopy));

        AskBuilder askBuilder = new AskBuilder();
        askBuilder.addWhere("<http://example.com/ns#requiredPropertyForPerson>",
                "<http://www.w3.org/ns/shacl#closed>",
                "\"true\"^^<http://www.w3.org/2001/XMLSchema#boolean>");
        Query query = askBuilder.build();
        QueryExecution queryExecution = QueryExecutionFactory.create(query, firstCopy);
        assertTrue(queryExecution.execAsk());
    }

    @Test
    public void retrieveShapesFromS3() {
        when(fileConfiguration.getShapes()).thenReturn(S3_SHAPE_PATH);

        when(s3ModelService.getModel(S3_SHAPE_PATH)).thenReturn(Optional.of(getModelAsString(VALID_TURTLE_SHAPE_PATH)));

        ShapeService shapeService = new ShapeServiceImpl(fileConfiguration, configurationLoader);
        assertNotNull(shapeService.getCopy());
    }

}