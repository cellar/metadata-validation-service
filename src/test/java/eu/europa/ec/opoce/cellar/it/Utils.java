package eu.europa.ec.opoce.cellar.it;

import eu.europa.ec.opoce.cellar.TestUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.Lang;

import java.io.StringReader;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Path;

public final class Utils {

    private Utils() {}

    public static String loadPathInModelAndReturnJsonFormat(String resource, Lang readLang) {
        Model model = ModelFactory.createDefaultModel();
        model.read(new StringReader(TestUtils.getModelAsString(resource)), "", readLang.getLabel());
        StringWriter writer = new StringWriter();
        model.write(writer, Lang.RDFJSON.getLabel());
        return writer.toString();
    }
}
