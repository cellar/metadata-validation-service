package eu.europa.ec.opoce.cellar.exception;

import eu.europa.ec.opoce.cellar.configuration.ParserConfiguration;
import org.apache.jena.riot.RiotException;
import org.apache.jena.riot.system.ErrorHandler;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class ParserErrorHandlerTest {


    @Test
    public void testWarningNoException() {
        ParserConfiguration configuration = new ParserConfiguration();
        configuration.setExceptionPatterns(Collections.singletonList("exception"));
        ErrorHandler errorHandlerMock = mock(ErrorHandler.class);

        ParserErrorHandler handler = new ParserErrorHandler(configuration, errorHandlerMock);
        handler.warning("message", 1, 2);

        verify(errorHandlerMock, times(1)).warning("message", 1, 2);
    }

    @Test
    public void testWarningException() {
        Assertions.assertThrows(RiotException.class, () -> {
            ParserConfiguration configuration = new ParserConfiguration();
            configuration.setExceptionPatterns(Collections.singletonList("exception"));
            ErrorHandler errorHandlerMock = mock(ErrorHandler.class);

            ParserErrorHandler handler = new ParserErrorHandler(configuration, errorHandlerMock);
            handler.warning("exception", 1, 2);
        });
    }

    @Test
    public void testError() {
        ParserConfiguration configuration = new ParserConfiguration();
        ErrorHandler errorHandlerMock = mock(ErrorHandler.class);

        ParserErrorHandler handler = new ParserErrorHandler(configuration, errorHandlerMock);
        handler.error("message", 1, 2);

        verify(errorHandlerMock, times(1)).error("message", 1, 2);
    }

    @Test
    public void testFatal() {
        ParserConfiguration configuration = new ParserConfiguration();
        ErrorHandler errorHandlerMock = mock(ErrorHandler.class);

        ParserErrorHandler handler = new ParserErrorHandler(configuration, errorHandlerMock);
        handler.fatal("message", 1, 2);

        verify(errorHandlerMock, times(1)).fatal("message", 1, 2);
    }
}