package eu.europa.ec.opoce.cellar.exception;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class S3DataExceptionTest {

    @Test
    public void test() {
        S3DataException exception = new S3DataException("message", new Exception("exception"));
        assertEquals("message", exception.getMessage());
        assertEquals("exception", exception.getCause().getMessage());
    }
}