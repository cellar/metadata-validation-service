package eu.europa.ec.opoce.cellar.service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import eu.europa.ec.opoce.cellar.exception.S3DataException;
import org.apache.http.client.methods.HttpRequestBase;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.io.ClassPathResource;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class S3ModelServiceImplTest {

    @Mock
    private AmazonS3 amazonS3;

    private S3ModelService s3ModelService;

    @BeforeEach
    public void init() {
        s3ModelService = new S3ModelServiceImpl(amazonS3);
    }

    @Test
    public void getModelFromEmptyPath() {
        assertFalse(s3ModelService.getModel("").isPresent());
    }

    @Test
    public void getModel() throws IOException {
        final String path = "https://s3-eu-west-1.amazonaws.com/test-bucket/test/test.txt";
        try (InputStream is = new ClassPathResource("/eu/europa/ec/opoce/cellar/test/shape/impl/empty-file.ttl").getInputStream()) {
            final HttpRequestBase httpRequestBase = mock(HttpRequestBase.class);
            final S3ObjectInputStream s3ObjectInputStream = new S3ObjectInputStream(is, httpRequestBase);

            final S3Object s3Object = mock(S3Object.class);
            when(s3Object.getObjectContent()).thenReturn(s3ObjectInputStream);

            when(amazonS3.getObject("test-bucket", "test/test.txt")).thenReturn(s3Object);

            assertTrue(s3ModelService.getModel(path).isPresent());
        }
    }

    @Test
    public void getModelWithThrownException() {
        Assertions.assertThrows(S3DataException.class, () -> {
            final String path = "https://s3-eu-west-1.amazonaws.com/test-bucket/test/test.txt";
            try (InputStream is = new ClassPathResource("/eu/europa/ec/opoce/cellar/test/shape/impl/empty-file.ttl").getInputStream()) {
                is.close();
                final HttpRequestBase httpRequestBase = mock(HttpRequestBase.class);
                final S3ObjectInputStream s3ObjectInputStream = new S3ObjectInputStream(is, httpRequestBase);
                final S3Object s3Object = mock(S3Object.class);
                when(s3Object.getObjectContent()).thenReturn(s3ObjectInputStream);
                when(amazonS3.getObject("test-bucket", "test/test.txt")).thenReturn(s3Object);
                s3ModelService.getModel(path);
            }
        });

    }
}